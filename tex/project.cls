\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{project}[2023/04/11 min klasse til projekter]

% Class options
\newif\ifclassdansk\classdanskfalse
\newif\ifclassfarver\classfarverfalse

\DeclareOption{dansk}{\classdansktrue}
\DeclareOption{farver}{\classfarvertrue}
% Any options passed to skeie.cls not "dansk" or "farver"
% will be passed to amsart instead.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{amsart}}
\ProcessOptions\relax

% Load amsart
\LoadClass[a4paper]{amsart}
\RequirePackage[utf8]{inputenc}

% Selecting the language
\ifclassdansk
	\PassOptionsToClass{danish}{amsart}
	\RequirePackage[danish]{babel}
\else
	\PassOptionsToClass{english}{amsart}
	\RequirePackage[english]{babel}
\fi

% ==================================================================
% ============================ PACKAGES ============================
% ==================================================================

\RequirePackage{etoolbox}
\RequirePackage{csquotes}
\RequirePackage{marginnote}
\RequirePackage{microtype}

\RequirePackage{enumitem}
\RequirePackage[nodayofweek]{datetime} % option "level", if you want st,nd,rd,th \emph{level} with the text
\RequirePackage{graphicx}

\RequirePackage{caption}
\RequirePackage{subcaption}

% Math packages
\RequirePackage{mathtools}
\RequirePackage{amsthm}
\RequirePackage{thmtools}
\RequirePackage{amssymb} % loads amsfonts
\RequirePackage{amscd}
\RequirePackage{bm}
\RequirePackage{mathrsfs} % for the calligraphy some people use for categories
\RequirePackage{siunitx} % for (Danish) numbers
\RequirePackage{CJKutf8} % For the Yoneda embedding (\yo)

\RequirePackage{ifthen}
\ifclassfarver
	\RequirePackage{xcolor}
\fi

\RequirePackage{hyperref}
\RequirePackage{cleveref} % should be loaded last

% ==================================================================
% =========================== CUSTOMIZATION ========================
% ==================================================================

\ifclassfarver
	\definecolor{KUrod}{HTML}{901A1E}
	\definecolor{KUgron}{HTML}{4b8325}
	\definecolor{KUblaa}{HTML}{122947}
	\hypersetup{
		colorlinks=true,
		citecolor={KUgron},
		linkcolor={KUrod},
		urlcolor={KUblaa},
		linktoc=page,
	}
\fi

% Language setup
\ifclassdansk
	\renewcommand{\datename}{\textit{Dato}:}
	\sisetup{locale = DE}% for siunitx
	\AfterEndPreamble{\renewcommand{\appendixname}{Appendiks}}
\else
	\renewcommand{\datename}{\textit{Date}:}
\fi

% changes the look of ToC
\def\l@section{\@tocline{1}{5pt}{1pc}{}{\bfseries}}
\def\l@subsection{\@tocline{2}{0pt}{2.7pc}{5pc}{}}

% page setup
% frontmatter command
\providecommand{\frontmatter}{
	\pagenumbering{roman}
}
% mainmatter command
\providecommand{\mainmatter}{
	\cleardoublepage
	\pagenumbering{arabic}
}

% styling the sections
\def\section{%
	\@startsection{section}{1}%
	\z@\z@{2\linespacing}%
	{\vspace*{1cm}\huge\normalfont\scshape\centering\thispagestyle{plain}}%
}
\preto{\section}{\newpage}{}{}% adds \newpage before every \section

\def\subsection{\@startsection{subsection}{1}%
	\z@{.7\linespacing\@plus\linespacing}{.5\linespacing}%
	{\normalfont\scshape\centering}%
}

% ==========================================================
% ====================== THEOREM-LIKE ======================
% ==========================================================

\newskip\minegenskip
\minegenskip=.5\baselineskip plus.2\baselineskip minus.2\baselineskip

\numberwithin{equation}{section}
\newcounter{mythmlike}
\numberwithin{mythmlike}{subsection}

\declaretheoremstyle[
	spaceabove=\minegenskip,
	spacebelow=\minegenskip,
	sibling=mythmlike
]{mindefinition}
\declaretheorem[style=mindefinition,name=Definition,qed=$\circ$]{definition}
\declaretheorem[style=mindefinition,name=Definition,qed=$\circ$,numbered=no]{definition*}
\ifclassdansk
	\declaretheorem[style=mindefinition,name=Eksempel]{eksempel}
	\declaretheorem[style=mindefinition,name=Eksempel,numbered=no]{eksempel*}
	\declaretheorem[style=mindefinition,name=Konstruktion]{konstruktion}
	\declaretheorem[style=mindefinition,name=Konstruktion,numbered=no]{konstruktion*}
\else
	\declaretheorem[style=mindefinition,name=Warning,numbered=no]{warning}
	\declaretheorem[style=mindefinition,name=Example]{example}
	\declaretheorem[style=mindefinition,name=Example,numbered=no]{example*}
	\declaretheorem[style=mindefinition,name=Construction]{construction}
	\declaretheorem[style=mindefinition,name=Construction,numbered=no]{construction*}
	\declaretheorem[style=mindefinition,name=Algorithm,numbered=no]{algorithm}
\fi

\declaretheoremstyle[
	spaceabove=\minegenskip,
	spacebelow=\minegenskip,
	bodyfont=\itshape,
	sibling=mythmlike,
]{minsetning}

\ifclassdansk
	\declaretheorem[style=minsetning,name=Sætning,numbered=yes]{theorem}
	\declaretheorem[style=minsetning,name=Sætning,numbered=no]{theorem*}
	\declaretheorem[style=minsetning,name=Korollar,numbered=no]{korollar}
	\declaretheorem[style=minsetning,name=Porisme,numbered=no]{porisme}
\else
	\declaretheorem[style=minsetning,name=Theorem,numbered=yes]{theorem}
	\declaretheorem[style=minsetning,name=Theorem,numbered=no]{theorem*}
	\declaretheorem[style=minsetning,name=Corollary,numbered=yes]{corollary}
	\declaretheorem[style=minsetning,name=Corollary,numbered=no]{corollary*}
	\declaretheorem[style=minsetning,name=Porism,numbered=no]{porism}
\fi

\declaretheorem[style=minsetning,name=Lemma,numbered=yes]{lemma}
\declaretheorem[style=minsetning,name=Lemma,numbered=no]{lemma*}
\declaretheorem[style=minsetning,name=Proposition]{proposition}
\declaretheorem[style=minsetning,name=Proposition,numbered=no]{proposition*}

\declaretheoremstyle[
	spaceabove=\minegenskip,
	spacebelow=\minegenskip,
	numbered=no,
	headfont=\itshape
]{minremark}

\ifclassdansk
	\declaretheorem[style=minremark,name=Bemærk,numbered=yes]{remark}
	\declaretheorem[style=minremark,name=Bemærk,numbered=no]{remark*}
	\declaretheorem[style=minremark,name=Påstand,numbered=yes]{claim}
	\declaretheorem[style=minremark,name=Påstand]{claim*}
\else
	\declaretheorem[style=minremark,name=Remark,numbered=yes]{remark}
	\declaretheorem[style=minremark,name=Remark,numbered=no]{remark*}
	\declaretheorem[style=minremark,name=Claim,numbered=yes]{claim}
	\declaretheorem[style=minremark,name=Claim]{claim*}
\fi

\declaretheorem[style=minremark,name=Vista]{vista}
\declaretheorem[style=minremark,name=Notation]{notation}
\declaretheorem[style=minremark,name=Motivation]{motivation}

\ifclassdansk
	\declaretheorem[style=minremark,name=Bevis af påstand,qed=$\square_{\text{påstand}}$]{proofofclaim}
\else
	\declaretheorem[style=minremark,name=Proof of claim,qed=$\square_{\text{claim}}$]{proofofclaim}
\fi

% lists
\newlist{nlist}{enumerate}{1}
\setlist[nlist]{label=\textnormal{\textbf{({\arabic*})}}}
\newlist{liste}{enumerate}{1}
\setlist[liste]{label=\textnormal{\textbf{\textit{\roman*})}}}
\newlist{sublist}{enumerate}{1}
\setlist[sublist]{label=\textnormal{\textbf{\alph*)}}}
