input skeie

outputtemplate := "figures/seifert-alg-step1.mps";
beginfig(1);

numeric afstand, radius, indreh, indrev;
afstand = 2u;
radius = u;
indrev = .1; % radius ratio on the left
indreh = .6; % radius ratio on the right

pair P[],indv[],indh[];

path bue[];

for i = 0 step 1 until 3:
	P[i] = (radius,0) rotated ((-45)-90*i);
	indv[i] = (radius,0) rotated ((-45)-90*i) scaled indrev;
	indh[i] = (radius,0) rotated ((-45)-90*i) scaled indreh;
endfor

% draw the circles
draw cirkel(origin,radius) shifted (-afstand,0);
draw cirkel(origin,radius) shifted ( afstand,0);

% draw the left knot
draw (P[2] -- P[0])    shifted (-afstand,0);
draw (P[1] -- indv[1]) shifted (-afstand,0);
draw (P[3] -- indv[3]) shifted (-afstand,0);
% add arrowheads
filldraw pil scaled (u*.8) rotated 45 shifted indh[3] shifted (-afstand,0);
filldraw pil scaled (u*.8) rotated 315 shifted indh[0] shifted (-afstand,0);

% draw right knot
for i = 0 step 1 until 3:
	draw (P[i] -- indh[i]) shifted (afstand,0);
endfor
bue[1] = (indh[0] {dir( 135)} .. indh[1]) shifted (afstand,0);
bue[2] = (indh[3] {dir(-135)} .. indh[2]) shifted (afstand,0);

draw bue[1];
draw bue[2];

% add arrowheads
filldraw pil scaled (u*.8) shifted (point .5 of bue[1]);
filldraw pil scaled (u*.8) shifted (point .5 of bue[2]);

% draw arrow from left to right
draw (-(afstand - radius)*.5,0) -- ((afstand - radius)*.5,0);
filldraw pil scaled (u*.8) shifted ((afstand - radius)*.5,0);

endfig;

outputtemplate := "figures/seifert-alg-step-end.mps";
beginfig(5);

numeric afstand, radius, indreh, indrev;
afstand = 2u;
radius = u;
indrev = .1; % radius ratio on the left
indreh = .6; % radius ratio on the right

pair P[],indv[],indh[];

path bue[];

for i = 0 step 1 until 3:
	P[i] = (radius,0) rotated ((-45)-90*i);
	indv[i] = (radius,0) rotated ((-45)-90*i) scaled indrev;
	indh[i] = (radius,0) rotated ((-45)-90*i) scaled indreh;
endfor

% define arcs
bue[1] = (indh[0] {dir( 135)} .. indh[1]) shifted (-afstand,0); % this is the bottom one
bue[2] = (indh[3] {dir(-135)} .. indh[2]) shifted (-afstand,0); % this is the top one

% draw the circles
draw cirkel(origin,radius) shifted (-afstand,0);
draw cirkel(origin,radius) shifted ( afstand,0);

% draw left knot
for i = 0 step 1 until 3:
	draw (P[i] -- indh[i]) shifted (-afstand,0);
endfor

draw bue[1];
draw bue[2];

% add arrowheads
filldraw pil scaled (u*.8) shifted (point .5 of bue[1]);
filldraw pil scaled (u*.8) shifted (point .5 of bue[2]);

% add orientation labels
label(TEX("$+$"),.5*((-afstand, radius)+(point .5 of bue[2])));
label(TEX("$-$"),.5*((-afstand,-radius)+(point .5 of bue[1])));

% draw right knot
draw (P[2] -- P[0])      shifted (afstand,0);
draw (P[1] -- indh[1])   shifted (afstand,0);
draw (indh[1] -- origin) shifted (afstand,0) dashed evenly;
draw (P[3] -- origin)    shifted (afstand,0);

% draw the arc
draw bue[2] shifted (afstand,0) rotated 90 shifted (afstand,0);

% add arrowheads
filldraw pil scaled (u*.8) rotated  45 shifted indh[3] shifted (afstand,0);
filldraw pil scaled (u*.8) rotated 315 shifted indh[0] shifted (afstand,0);

% add orientation labels
label(TEX("$+$"),.5*((afstand, radius)+(point .5 of bue[2])) shifted (afstand,0));
label(TEX("$-$"),.5*((afstand,-radius)+(point .5 of bue[1])) shifted (afstand,0));

% draw arrow from left to right
draw (-(afstand - radius)*.5,0) -- ((afstand - radius)*.5,0);
filldraw pil scaled (u*.8) shifted ((afstand - radius)*.5,0);

endfig;

outputtemplate := "figures/trefoil-step1.mps";
beginfig(2);

input trefoil;

path arm_shortened[],edge_shortened[];

% define arm_shortened and edge_shortened
for i = 0 step 1 until 2:
	arm_shortened[i] = subpath (0,0.985) of arm[i];
	edge_shortened[i] = subpath (.06,1) of triangleEdge[i];
endfor

% draw arms
for i = 0 step 1 until 2:
	draw arm_shortened[i];
	draw edge_shortened[i];
endfor

% add arrows
for i = 0 step 1 until 2:
	filldraw pil scaled (u*.8) rotated angle(direction .5 of triangleEdge[i]) shifted (point .5 of triangleEdge[i]);
endfor

endfig;

outputtemplate := "figures/trefoil-step2.mps";
beginfig(3);

input trefoil;

% draw discs
draw disc[1];
draw disc[2];

% add arrowheads
for i = 0 step 1 until 2:
	filldraw pil scaled (u*.8) rotated angle(direction 0.5 of triangleEdge[i]) shifted (point .5 of triangleEdge[i]);
	filldraw pil scaled (u*.8) rotated angle(direction 0.5 of arm[i]) shifted (point .5 of arm[i]);
endfor

% add orientation labels
label(TEX("$+$"),origin);
label(TEX("$+$"),.5*((point .5 of arm[1])+(point .5 of triangleEdge[2])));
endfig;

outputtemplate := "figures/trefoil-step3.mps";
beginfig(4);

input trefoil;

% draw the discs
filldraw disc[1] transformed T[2] withcolor .7white;
draw disc[2] transformed T[2];
filldraw disc[1] transformed T[1] withcolor white;
draw disc[1] transformed T[1];

% draw arrowheads
for i = 0 step 1 until 2:
	% on disc 1
	filldraw pil scaled (u*.8) rotated angle(direction 0.5 of triangleEdge[i]) shifted (point .5 of triangleEdge[i]) transformed T[1];
	% on disc 2
	filldraw pil scaled (u*.8) rotated angle(direction 0.5 of arm[i]) shifted (point .5 of arm[i]) transformed T[2];
endfor

% add orientation labels
label(TEX("$+$"),origin) scaled .7 transformed T[1];
label(TEX("$+$"),origin) scaled .7 shifted (.5*((point .5 of arm[1])+(point .5 of triangleEdge[2]))) transformed T[2];

endfig;

outputtemplate := "figures/trefoil-step4.mps";
beginfig(6);

input trefoil;

path boundary[],fold_line[];
pair tangent_point[];

inner_time := .2;
outer_time := .1;

boundary[0] = (subpath (0,1-outer_time) of arm[0]) .. subpath (inner_time,1) of reverse triangleEdge[1];
boundary[1] = boundary[0] rotated 120;
boundary[2] = boundary[1] rotated 120;

tangent_point[0] = point (directiontime dir(120) of arm[0]) of arm[0];
tangent_point[1] = tangent_point[0] xscaled -1 rotated -120;
fold_line[0] = tangent_point[0] -- tangent_point[1];
fold_line[1] = fold_line[0] rotated 120;
fold_line[2] = fold_line[1] rotated 120;

filldraw fold_line[2] -- fold_line[1] -- fold_line[0] -- cycle withcolor color_negative;
for i = 0 step 1 until 2:
	filldraw boundary[i] -- cycle withcolor color_positive;
endfor
filldraw triangleVertex[0] -- triangleVertex[1] -- triangleVertex[2] -- cycle withcolor color_positive;

for i = 0 step 1 until 2:
	draw boundary[i] withpen fold_pen;
	draw triangleEdge[i] withpen knot_pen;
	draw subpath (0,1-outer_time) of arm[i] withpen knot_pen;
	draw subpath (1-inner_time,1) of triangleEdge[i] withpen knot_pen;
	draw subpath (1-outer_time,1) of arm[i] dashed evenly withpen knot_pen;
	draw fold_line[i] withpen fold_pen;
endfor

% add orientation labels
% label(TEX("$+$"),origin);
for i = 0 step 1 until 2:
	% label(TEX("$-$"),.5(triangleVertex[i+1]+(point .5 of fold_line[i])));
	% label(TEX("$+$"),.5((point .5 of triangleEdge[i+1])+(point .5 of arm[i])));
	% add arrowheads
	filldraw pil scaled (.8*u) rotated angle(direction .5 of triangleEdge[i]) shifted (point .5 of triangleEdge[i]);
	filldraw pil scaled (.8*u) rotated angle(direction .5 of arm[i]) shifted (point .5 of arm[i]);
endfor

filldraw unitsquare scaled (.5*u) shifted (2u,2u)   withcolor color_positive;
filldraw unitsquare scaled (.5*u) shifted (2u,1.4u) withcolor color_negative;
draw unitsquare scaled (.5*u) shifted (2u,2u)  ;
draw unitsquare scaled (.5*u) shifted (2u,1.4u);

label(TEX("$+$"),((2u,2u  )+(.25u,.25u)));
label(TEX("$-$"),((2u,1.4u)+(.25u,.25u)));

endfig;

input cmarrows;

setup_cmarrows(
	brace_name     = "extensiblebrace";
	parameter_file = "cmr10.mf";
	macro_name     = "brace");

outputtemplate := "figures/connecting-homomorphism.mps";
beginfig(7);

	numeric radius_meridian, radius, radius_offset, sigma_length, sigma_time[], sigma_depth, dashed_factor, scale, tubular_time[];
	path meridian, sigma[], sigma_region[], tubular_NBHD, sigma_arc[];
	pair X,Y;

	scale = 1.5;
	radius = 1u * scale;
	radius_meridian = 1.5u * scale;
	radius_offset = .2u * scale;
	sigma_length = 3u * scale;
	sigma_depth = .3u * scale;
	dashed_factor = .2;

	X = (radius_meridian,0) rotated 154;
	Y = X yscaled -1;

	% define meridian path
	meridian = cirkel(origin, radius_meridian);
	tubular_NBHD = cirkel(origin, radius);

	sigma_arc[1] = subpath (directiontime dir(154+90) of meridian, directiontime dir(360-154+90) of meridian) of meridian;
	sigma_arc[2] = subpath (directiontime dir(360-154+90) of meridian,8) of meridian -- subpath (0,directiontime dir(154+90) of meridian) of meridian;

	% define simas
	sigma[0] = (-sigma_length,0) -- origin;

	sigma[1] = sigma[0] shifted (0, radius - sigma_depth - radius_offset);
	sigma[2] = sigma[0] shifted (0, radius - radius_offset);

	for i = 1 step 1 until 2:
		sigma[-i] = sigma[i] yscaled -1;
	endfor

	% define tubular_times -- the times for the intersections of tubular_NBHD and sigma[]
	for i = -2 step 1 until 2:
		(tubular_time[i],sigma_time[i]) = tubular_NBHD intersectiontimes sigma[i];
	endfor

	% define sigma regions
	sigma_region[1] = subpath (0,sigma_time[1]) of sigma[1] -- (reverse(subpath (tubular_time[2],tubular_time[1]) of tubular_NBHD)) -- reverse(subpath (0,sigma_time[2]) of sigma[2]) -- cycle;
	sigma_region[-1] = sigma_region[1] yscaled -1;

	% color sigma_regions
	filldraw sigma_region[1] withcolor .7 white;
	filldraw sigma_region[-1] withcolor .7 white;

	% draw sigmas
	for i = -2 step 1 until 2:
		draw sigma[i];
		% draw subpath (0,dashed_factor) of sigma[i] dashed evenly;
	endfor

	% draw dashed line (indicating open)
	draw (point 1 of sigma[2]) -- (point 1 of sigma[-2]);

	% draw arcs i.e. the 1-cycles
	% draw sigma_arc[1] withcolor blue;
	% draw sigma_arc[2] withcolor red;
	draw meridian;

	% add arrows
	filldraw pil scaled (u*.8) rotated angle(direction 0 of meridian) shifted (point 0 of meridian);
	filldraw pil scaled (u*.8) rotated (170+90) shifted ((radius_meridian,0) rotated 170);

	% add labels
	dotlabel.rt(TEX("$K$"),origin);
	label(TEX("$\nu(K)$"),(radius * .6,0) rotated 40);
	label.rt(TEX("$\sigma_2$"),(radius_meridian,0));
	label.lft(TEX("$\sigma_1$"),(radius_meridian,0) rotated 170);
	dotlabel.lft(TEX("$x$"),X);
	dotlabel.lft(TEX("$y$"),Y);
	label.top(TEX("$\Sigma_{g}^{+}$"),point .5 of sigma[2]);
	label.bot(TEX("$\Sigma_{g}^{-}$"),point .5 of sigma[-2]);
	label.rt(TEX("$m=\sigma_1 + \sigma_2$"), (radius_meridian * 1.1,0) rotated 45);
	label(TEX("$\bm{\Sigma}^+$"),origin) scaled .7 shifted (X - (2u,0));
	label(TEX("$\bm{\Sigma}^-$"),origin) scaled .7 shifted (Y - (2u,0));

	brace ((point 0 of sigma[-2]) -- (point 0 of sigma[2])) shifted (-.1u,0);
	label.lft(TEX("$\bm{\Sigma}$"),point 0 of sigma[0] shifted (-.3u,0));

	draw cirkel(origin, radius);

endfig;
end;
