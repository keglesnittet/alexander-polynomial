%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -86 -29 86 29 
%%HiResBoundingBox: -85.28935 -28.59645 85.28935 28.59645 
%%Creator: MetaPost 2.02
%%CreationDate: 2024.01.19:2316
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinejoin 10 setmiterlimit
newpath -28.34645 0 moveto
-28.34645 7.51828 -31.33353 14.72774 -36.64934 20.04356 curveto
-41.96516 25.35938 -49.17462 28.34645 -56.6929 28.34645 curveto
-64.21118 28.34645 -71.42064 25.35938 -76.73647 20.04356 curveto
-82.05228 14.72774 -85.03935 7.51828 -85.03935 0 curveto
-85.03935 -7.51828 -82.05228 -14.72774 -76.73647 -20.04356 curveto
-71.42064 -25.35938 -64.21118 -28.34645 -56.6929 -28.34645 curveto
-49.17462 -28.34645 -41.96516 -25.35938 -36.64934 -20.04356 curveto
-31.33353 -14.72774 -28.34645 -7.51828 -28.34645 0 curveto closepath stroke
newpath 85.03935 0 moveto
85.03935 7.51828 82.05228 14.72774 76.73647 20.04356 curveto
71.42064 25.35938 64.21118 28.34645 56.6929 28.34645 curveto
49.17462 28.34645 41.96516 25.35938 36.64934 20.04356 curveto
31.33353 14.72774 28.34645 7.51828 28.34645 0 curveto
28.34645 -7.51828 31.33353 -14.72774 36.64934 -20.04356 curveto
41.96516 -25.35938 49.17462 -28.34645 56.6929 -28.34645 curveto
64.21118 -28.34645 71.42064 -25.35938 76.73647 -20.04356 curveto
82.05228 -14.72774 85.03935 -7.51828 85.03935 0 curveto closepath stroke
 1 setlinecap
newpath -76.7369 20.04399 moveto
-36.64891 -20.04399 lineto stroke
newpath -76.7369 -20.04399 moveto
-58.69742 -2.00452 lineto stroke
newpath -36.64891 20.04399 moveto
-54.68839 2.00452 lineto stroke
newpath -44.66638 12.02652 moveto
-44.66638 10.42291 lineto
-43.06276 13.63014 lineto
-46.26999 12.02652 lineto
 closepath
gsave fill grestore stroke
newpath -44.66638 -12.02652 moveto
-46.26999 -12.02652 lineto
-43.06276 -13.63014 lineto
-44.66638 -10.42291 lineto
 closepath
gsave fill grestore stroke
newpath 76.7369 -20.04399 moveto
68.71942 -12.02652 lineto stroke
newpath 36.64891 -20.04399 moveto
44.66638 -12.02652 lineto stroke
newpath 36.64891 20.04399 moveto
44.66638 12.02652 lineto stroke
newpath 76.7369 20.04399 moveto
68.71942 12.02652 lineto stroke
newpath 68.71942 -12.02652 moveto
62.07736 -5.38446 51.30844 -5.38446 44.66638 -12.02652 curveto stroke
newpath 68.71942 12.02652 moveto
62.07736 5.38446 51.30844 5.38446 44.66638 12.02652 curveto stroke
newpath 56.69289 -7.04498 moveto
55.55896 -8.17891 lineto
58.96075 -7.04498 lineto
55.55896 -5.91106 lineto
 closepath
gsave fill grestore stroke
newpath 56.69289 7.04498 moveto
55.55896 5.91106 lineto
58.96075 7.04498 lineto
55.55896 8.17891 lineto
 closepath
gsave fill grestore stroke
newpath -14.17323 0 moveto
14.17323 0 lineto stroke
newpath 14.17323 0 moveto
13.0393 -1.13393 lineto
16.4411 0 lineto
13.0393 1.13393 lineto
 closepath
gsave fill grestore stroke
showpage
%%EOF
