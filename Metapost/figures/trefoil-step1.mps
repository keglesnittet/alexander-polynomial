%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -55 -47 55 57 
%%HiResBoundingBox: -54.80296 -46.09457 54.80304 56.94296 
%%Creator: MetaPost 2.02
%%CreationDate: 2024.01.19:2316
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 24.54881 14.17323 moveto
56.78947 70.01585 -53.87688 70.85345 -25.92325 16.68604 curveto stroke
newpath 1.47285 -25.79538 moveto
24.54881 14.17323 lineto stroke
newpath -24.54883 14.17332 moveto
-89.03041 14.17334 -34.42264 -82.08562 -1.48894 -30.79327 curveto stroke
newpath 21.60306 14.17323 moveto
-24.54883 14.17332 lineto stroke
newpath -0.00008 -28.34663 moveto
32.2407 -84.1895 88.29974 11.23184 27.41232 14.10718 curveto stroke
newpath -23.07597 11.62222 moveto
-0.00008 -28.34663 lineto stroke
newpath 12.27443 -7.0866 moveto
12.68947 -8.63557 lineto
13.40837 -5.12256 lineto
10.72545 -7.50163 lineto
 closepath
gsave fill grestore stroke
newpath -0.00002 14.1733 moveto
1.13391 15.30722 lineto
-2.26788 14.1733 lineto
1.13391 13.03937 lineto
 closepath
gsave fill grestore stroke
newpath -12.27444 -7.08667 moveto
-13.82343 -6.67163 lineto
-11.1405 -9.0507 lineto
-11.8594 -5.53769 lineto
 closepath
gsave fill grestore stroke
showpage
%%EOF
