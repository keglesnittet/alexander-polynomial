\section{The Alexander Module}
\label{sec:the_alexander_module}

In this section we will construct the \emph{Alexander module}, which is a knot invariant, that we will use in the construction of the Alexander polynomial.
It is worth noting, that we actually remain in the \emph{topological} case in this section (replacing $X_{K}$ by simply $K^{\complement}$).

\subsection{Reminder on Deck Transformations}
\label{ssec:reminder_on_coverings}

This section closely follows the exposition given in Chapter 1 of \cite{bog:Hatcher}.

\begin{definition}
	We call a covering, $p\colon \tilde{X}\to X$, \emph{regular} if $p_{*}\pi_{1}(\tilde{X},\tilde{x})$ is a normal subgroup of $\pi_{1}(X,p(\tilde{x}))$ for all $\tilde{x}\in\tilde{X}$.
\end{definition}

\begin{definition}
	Let $X$ be a topological space.
	Denote by $\Cov(X)$ the category with objects covering maps $p\colon \tilde{X}\to X$ and morphisms
	\begin{equation*}
		f\colon (p\colon \tilde{X}\to X) \to (p'\colon \tilde{X}'\to X)
	\end{equation*}
	the morphisms $f\colon \tilde{X}\to\tilde{X}'$ in $\Top$ such that the following triangle commutes
	\begin{equation*}
		\begin{tikzcd}
			{\tilde{X}} &   & {\tilde{X}'} \\
						& X\point
			\arrow["p"', from=1-1, to=2-2]
			\arrow["{p'}", from=1-3, to=2-2]
			\arrow["f", from=1-1, to=1-3]
		\end{tikzcd}
	\end{equation*}
	We will call $\Cov(X)$ the \emph{category of coverings} of $X$.
\end{definition}

We leave it to the reader to check that $\Cov(X)$ is a category for all $X\in\Top$.

\begin{definition}
	Let $p\colon \tilde{X}\to X$ be a covering map.
	A \emph{deck transformation} of $p$ is an isomorphism $p\cong p$ (in $\Cov(X)$). %$f\colon \tilde{X}\to\tilde{X}$ such that $$
	We denote by $\Deck(p)$ the group of deck transformations of $p$ under composition i.e.\ $\Deck(p)\coloneqq \Aut_{\Cov(X)}(p)$.
\end{definition}

\begin{notation}
	For a group $G$ we denote by $\B G$ the category with one object, $*$, and morphisms $G$.
\end{notation}

\begin{definition}
	Let $G$ be a group and $X$ a topological space.
	A (\emph{continuous}) \emph{action} of $G$ on $X$ is a functor $\B G\to \Top$ which sends $*$ to $X$.
\end{definition}

For any covering map $p\colon \tilde{X}\to X$ there is an obvious (continuous) action of the group $\Deck(p)$ on $\tilde{X}$, namely the action $\B\Deck(p) \to \Top$ sending $*$ to $\tilde{X}$ and every $f\in\Deck(p)$ to ``itself'' i.e.\ to $f\colon \tilde{X} \to \tilde{X}$.

\begin{lemma}\label{lem:existence_of_deck_transformations}
	Let $X\in\Top$ be a path-connected, locally path-connected space, let $x\in X$ and let $p,p'\in\Cov(X)$ be path-connected coverings of $X$.
	If $\tilde{x}$ and $\tilde{x}'$ are points in the fibres of $p$ and $p'$ over $x$ respectively then there exists an isomorphism $f\colon p\to p'$ taking $\tilde{x}$ to $\tilde{x}'$ if and only if the images of the induced maps of $p$ and $p'$ on fundamental groups are equal.
\end{lemma}

\begin{proof}
	Denote the coverings by $p\colon (\tilde{X},\tilde{x})\to (X,x)$ and $p'\colon (\tilde{X}',\tilde{x}')\to (X,x)$.
	If there exists an isomorphism $f\colon p\to p'$ then $\im p_{*}=\im p'_{*}$ by \cite[Theorem 2.7.2]{bog:AlgTopI}.
	Conversely assume $\im p_{*}=\im p'_{*}$.
	Since $\im p_{*}=\im p'_{*}$ there exist unique lifts $\tilde{p}\colon \tilde{X}\to\tilde{X}'$ and $\tilde{q}\colon \tilde{X}'\to \tilde{X}$ of $p$ and $p'$ respectively (by e.g.\ \cite[Theorem 2.7.2]{bog:AlgTopI}).
	By uniqueness of these lifts we now have $\tilde{p} \circ \tilde{p}' =\id_{\tilde{X}'}$ and $\tilde{p}' \circ \tilde{p} = \id_{\tilde{X}}$ so $\tilde{p}$ is an isomorphism $p\to p'$ such that $\tilde{p}(\tilde{x})= \tilde{x}'$ and we are done.
\end{proof}

\begin{lemma}\label{lem:basepoint_change_is_conjugation} 
	Let $p\colon (\tilde{X}, \tilde{x})\to(X,x)$ be a path-connected covering and let $\tilde{x}'$ be a point in the fibre of $p$.
	Then the subgroup $p_{*}\pi_{1}(\tilde{X},\tilde{x}')$ is conjugate to $p_{*}\pi_{1}(\tilde{X},\tilde{x})$ in $\pi_{1}(X,x)$.
\end{lemma}

\begin{proof}
	Pick a path $\mu\colon \tilde{x} \pathto \tilde{x}'$.
	The map $\mu_{*}\colon \pi_{1}(\tilde{X},\tilde{x}') \to \pi_{1}(\tilde{X},\tilde{x})$ is an isomorphism.
	Since both $\tilde{x}$ and $\tilde{x}'$ are in the fibre of $p$, $p\circ\mu$ must be a loop at $x$.
	Let $[\lambda]\in\pi_{1}(\tilde{X},\tilde{x})$ and let $[\lambda']$ denote $(\mu_{*})^{-1}([\lambda])\in\pi_{1}(\tilde{X},\tilde{x}')$.
	Now 
	\begin{align*}
		p_{*}[\lambda]&=p_{*}(\mu_{*}([\lambda'])) = p_{*} ([\mu \cdot \lambda' \cdot \bar{\mu}]) = [p\circ(\mu \cdot \lambda' \cdot \bar{\mu})] = [p\circ\mu]\cdot [p\circ\lambda']\cdot[p\circ\bar{\mu}]\\
					  &=[p\circ\mu]\cdot p_{*}([\lambda'])\cdot[p\circ\mu]^{-1}
	\end{align*}
	and so, since $p_{*}$ is injective, we have $p_{*}\pi_{1}(\tilde{X},\tilde{x})=[p\circ\mu](p_{*}\pi_{1}(\tilde{X},\tilde{x}'))[p\circ\mu]^{-1}$.
	This proves the lemma.
\end{proof}

\begin{lemma}\label{lem:determined_by_one_point}
	Deck transformations of path-connected coverings are uniquely determined by their image of any single point.
\end{lemma}

\begin{proof}
	Let $p\colon \tilde{X}\to X$ be a path-connected covering, $\tilde{x}\in\tilde{X}$ and $f,g\in\Deck(p)$.
	Assume $f(\tilde{x})=g(\tilde{x})$.
	We will now prove that $f=g$.
	Let $\tilde{x}'\in\tilde{X}$ and pick a path $\lambda\colon\tilde{x}\pathto\tilde{x}'$.
	By definition of $\Deck(p)$ we have that $p\circ\lambda=p\circ f\circ\lambda=p\circ g\circ\lambda$ so both $f\circ\lambda$ and $g\circ\lambda$ are lifts of $p\circ\lambda$.
	Now, by uniqueness of lifts, we have $f\circ\lambda=g\circ\lambda$ so, in particular, $f(\tilde{x}')=f(\lambda(1))=g(\lambda(1))=g(\tilde{x}')$ and we are done.
\end{proof}

\begin{remark*}
	In particular, only $\id\in\Deck(p)$ has any fix-points for any path-connected covering $p$.
\end{remark*}

\begin{proposition}\label{prop:calculation_of_deck}
	Let $X$ be a path-connected and locally path-connected topological space and let $p\colon \tilde{X}\to X$ be a path-connected, regular covering, then 
	\begin{equation*}
		\Deck(p) \cong \pi_{1}(X,p(\tilde{x}))/p_{*}\pi_{1}(\tilde{X},\tilde{x})
	\end{equation*}
	for all $\tilde{x}\in\tilde{X}$.
\end{proposition}

\begin{proof}
	Let $p\colon \tilde{X}\to X$ be a regular covering and let $\tilde{x}\in\tilde{X}$.
	We will define the function $\phi\colon \pi_{1}(X,p(\tilde{x})) \to \Deck(p)$ by $[\lambda]\mapsto \tau\in\Deck(p)$ defined by $\tau(\tilde{x})=p(\tilde{x}).[\lambda]$ for all $[\lambda]\in\pi_{1}(X,p(\tilde{x}))$ and check that $\phi$ is well-defined.
	For $\phi$ to be well-defined we only need to check that such a $\tau$ always exists and is well-defined.
	\Cref{lem:determined_by_one_point} ensures that such a $\tau$, if it exists, is well-defined and, because $\phi$ is regular, it follows from \cref{lem:basepoint_change_is_conjugation} and \cref{lem:existence_of_deck_transformations} that such a $\tau$ always exists, so $\phi$ is well-defined.
	The function $\phi$ is a homomorphism when restricting the deck transformations to the fibre of $p$, so by uniqueness $\phi$ is a homomorphism.
	Because $p$ is regular, \cref{lem:basepoint_change_is_conjugation} combined with \cref{lem:existence_of_deck_transformations} gives us that $\phi$ is surjective.
	It now suffices to show, that $\ker\phi=p_{*}\pi_{1}(\tilde{X},\tilde{x})$.
	By \cref{lem:determined_by_one_point} we get that $\phi([\lambda])=\id_{\tilde{X}}$ if and only if $p(\tilde{x}).[\lambda]=\tilde{x}$ i.e.\ if and only if $\lambda$ lifts to a loop in $\tilde{X}$ along $p$, but by \cite[corollary 2.2.10]{bog:AlgTopI} this is the case if and only if $[\lambda]\in p_{*}\pi_{1}(\tilde{X},\tilde{x})$ so we are done.
\end{proof}

\subsection{Defining the Alexander Module}
\label{ssec:defining_the_alexander_module}

In this section we will define the \emph{Alexander module}, which is a knot invariant.
We begin, by considering the following \namecref{thm:reduced_hom_compl_spheres} (which is \cite[theorem 9.4.2]{bog:AlgTopI}):

\begin{theorem}\label{thm:reduced_hom_compl_spheres}
	For any $k,n\in\N_{0}$ and any injective map $i\colon S^{k}\mono S^{n}$ we have 
	\begin{equation*}
		\tilde{H}_{*} (S^{n} \uden i(S^{k})) \cong
		\begin{cases}
			\Z & \text{for }*=n-k-1\\
			0  & \text{otherwise.}
		\end{cases}
	\end{equation*}
\end{theorem}

\begin{proof}
	Omitted.
\end{proof}

\begin{vista}
	This is not just a coincidence.
	It is reminiscent of \emph{Alexander duality}.
\end{vista}

From the above theorem, it follows, that for a knot $K\colon S^{n} \mono S^{n+2}$ the reduced homology of the complement $X_{K}$ is 
\begin{equation*}
	\tilde{H}_{i} (X_{K};\Z) \cong 
	\begin{cases}
		\Z & \text{for } i=1\\
		0 & \text{otherwise.}
	\end{cases}
\end{equation*}
In particular $X_{K}$ is path-connected and $H_{1}(X_{K};\Z)=\tilde{H}_{1}(X_{K};\Z) \cong \Z$ and all higher homology groups are trivial, so only the first homology group is of interest.%
\footnote{We will use later (without proof) that Alexander duality (\cref{thm:alexander_duality}) also gives us, that the meridian of $K$ is the generator of $H_{1}(X_{K};\Z)$.}

\begin{definition}
	A path-connected covering $p\colon \tilde{X} \to X$ is called an \emph{infinite cyclic cover} (of $X$) if $\Deck(p)\cong\Z$.
\end{definition}

\begin{remark*}
	Note that for an infinite cyclic cover of a space $X$, there is a bijection between $\Z$ and the fibre of the covering (at any point).
\end{remark*}

To ensure the existence of the infinite cyclic cover of a knot-complement even for non-surjective \emph{topological knots}, we will use the following point-set topological lemma, which will allow us to use the Galois correspondence for path-connected coverings.

\begin{lemma}
	For any non-surjective continuous map $K\colon S^{k} \to S^{n}$, the complement $S^{n}\uden K(S^{k})\neq \emptyset$ is locally path-connected and semi-locally simply connected.
\end{lemma}

\begin{proof}
	Since $S^{k}$ is compact so is $K(S^{k})$ and, since $S^{n}$ is Hausdorff, this means that $K(S^{k})$ is closed in $S^{n}$ and therefore $S^{n} \uden K(S^{k})$ is open in $S^{n}$.
	Pick a point $s\in S^{k}$.
	Now use stereographic projection to produce a homeomorphism $\phi\colon S^{n}\uden\{K(s)\}\to\R^{n}$.
	Using that $S^{n}$ is Hausdorff we get that $\{K(s)\}$ is closed in $S^{n}$ and therefore that $S^{n}\uden \{K(s)\}$ is open in $S^{n}$.
	This gives us, that $S^{n}\uden K(S^{k})$ is an open subset of $S^{n}\uden \{K(s)\}$.
	Since $S^{n}\uden K(S^{k})$ is an open subset of $S^{n}\uden \{K(s)\}$ we can restrict $\phi$ to a homeomorphism from $S^{n}\uden K(S^{k})$ to an open (non-empty since $K$ is not surjective) subset of $\R^{n}$.
	Open subsets of $\R^{n}$ are locally path-connected and locally contractible so they are \textit{a fortiori} also semi-locally simply connected.
\end{proof}

\begin{proposition}\label{prop:existence_of_infinite_cyclic_cover}
	If $K\colon S^{n} \mono S^{n+2}$ is a (resp.\ non-surjective topological) knot, then there exists a unique (up to isomorphism) infinite cyclic covering of $X_{K}$ (resp.\ $K^{\complement}$).
\end{proposition}

\begin{proof}
	Because $X_{K}$ is sufficiently nice, we can use the classification of path-connected coverings to get the existence of a unique covering $p\colon \tilde{X}_{K} \to X_{K}$ such that $p_{*}\pi_{1}(\tilde{X}_{K},\tilde{x})$ is the commutator subgroup of $\pi_{1}(X_{K},p(\tilde{x}))$ for all $\tilde{x}\in \tilde{X}_{K}$ (using that the commutator subgroup is normal).
	Now, since the commutator subgroup is normal, this covering is regular and so \cref{prop:calculation_of_deck} gives us, that 
	\begin{equation*}
		\Deck(p)\cong \pi_{1}(X,x) /\im p_{*} = \pi_{1}(X,x)^{\ab} \cong H_{1}(X_{K};\Z) \cong \Z
	\end{equation*}
	for all $x\in X_{K}$ where we use \cref{thm:reduced_hom_compl_spheres}.
	So $p$ is an infinite cyclic cover of $X_{K}$.
\end{proof}

\begin{notation}
	We will denote the domain of the (path-connected) infinite cyclic cover of $X_{K}$ by $\tilde{X}_{K}$ (which is, of course, also unique up to isomorphism).
\end{notation}

\begin{notation}
	We denote by $\Lambda$ the group ring $\Z[\Z]$, which is isomorphic to $\Z[t^{\pm1}]$ as a ring.
\end{notation}

\begin{lemma}\label{lem:lambda_module_stucture}% this should have been a construction
	For any knot $K\colon S^{n} \mono S^{n+2}$, the group $H_{1}(\tilde{X}_{K};\Z)$ has the structure of a $\Lambda$-module.
\end{lemma}

\begin{proof}
	Let $p\colon \tilde{X}_{K} \to X_{K}$ denote the infinite cyclic cover of $X_{K}$.
	The (continuous) action $\B\Deck(p)\to\Top$ of $\Deck(p)$ on $\tilde{X}_{K}$ gives us an action $\B\Deck(p)\to\Ab$ of $\Deck(p)$ on $H_{1}(\tilde{X}_{K};\Z)$ by post-composition with $H_{1}(-;\Z)\colon \Top\to\Ab$.
	Since $\Deck(p)\cong\Z$, this means we have an action of $\Z$ on $H_{1}(\tilde{X}_{K};\Z)$ and so, by \cref{lem:group_ring_isomorphism}, $H_{1}(\tilde{X}_{K};\Z)$ has the structure of a $\Z[\Z]$-module (i.e.\ specifically the one given by using $\phi$ from \cref{con:group_ring_isomorphism}).
\end{proof}

\begin{definition}
	Let $K\colon S^{n} \mono S^{n+2}$ be a knot.
	We will call $H_{1}(\tilde{X}_{K};\Z)$, with the $\Lambda$-module structure specified in the proof of \cref{lem:lambda_module_stucture}, the \emph{Alexander Module} (of $K$) and denote it by $\mathcal{A}(K)$.
\end{definition}

Clearly the Alexander module is a knot invariant.

\subsubsection{Group rings}
\label{sssec:group_rings}

This sections \emph{raison d'être} is to explain (in an overly-complicated way) exactly how we got the $\Z[\Z]$-module structure on $\A(K)$.

\begin{construction}\label{con:group_ring_isomorphism} 
	Let $G$ be a group and $R$ be a commutative, unital ring.
	Define functors 
	\begin{equation*}
		\phi\colon \Fun(\B G,\Mod_{R}) \leftrightarrows \Mod_{R[G]} \cocolon \psi
	\end{equation*}
	by $\phi(F)=F(*)$ with the $R[G]$-module structure given by $mg=F(g)(m)$ for all $F\colon \B G\to\Mod_{R}$ and $\phi(\alpha\colon F\Rightarrow H) = (\alpha_{*}\colon F(*) \to H(*))$ which becomes $R[G]$-linear by definition of natural transformations.

	Let $M\in\Mod_{R[G]}$ and define $\psi$ by $\psi(M)=(F\colon \B G\to \Mod_{R})$ defined by $F(*)=U(M)$ where $U$ is the restriction of scalars along the inclusion $R\mono R[G]$ and $F(g)\colon U(M) \to U(M)$ defined by $m\mapsto mg$.
\end{construction}

\begin{lemma}\label{lem:group_ring_isomorphism}
	Let $G$ be a group and $R$ be a commutative, unital ring, then the functors $\phi$ and $\psi$ from \cref{con:group_ring_isomorphism} induce an equivalence of categories 
	\begin{equation*}
		\phi\colon \Fun(\B G,\Mod_{R}) \cong \Mod_{R[G]}\cocolon \psi\point
	\end{equation*}
\end{lemma}

\begin{proof}
	The proof amounts to unravelling the definitions.
\end{proof}

\begin{remark*}
	In particular, spelling this out, we can get:
	Let $A\in\Mod_{R}$ with $R$ commutative and unital and let $G\in\Grp$.
	If $G$ acts on $A$, then $A$, naturally, has the structure of an $R[G]$-module.
\end{remark*}
