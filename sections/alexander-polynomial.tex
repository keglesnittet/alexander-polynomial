\section{The Alexander Polynomial}
\label{sec:the_alexander_polynomial}

In this section we will construct the Alexander polynomial of a knot.

\subsection{Glimpses of Algebra}
\label{ssec:glimpses_of_algebra}

Before constructing the Alexander polynomial we first take a quick foray into algebra, in order to define the \emph{order ideal}, which we need for our construction of the Alexander polynomial.
All rings in this project are commutative, unital rings.
We start out small with the following result which we will use to show, abstractly, that $\A(K)$ is finitely presented in \cref{sssec:Abstract_finite_presentedness_argument}.

\begin{lemma}\label{lem:lambda_is_noetherian} 
	The ring $\Lambda$ is Noetherian.
\end{lemma}

\begin{proof}
	The ring $\Z$ is Noetherian (it is Euclidean) so, by Hilberts Basis Theorem \cite[theorem 7.5]{bog:Atiyah_MacDonald}, $\Z[t]$ is Noetherian and so $(\Z[t])_{t}\cong \Z[t^{\pm1}]\cong\Lambda$ is Noetherian, by \cite[prop. 7.3]{bog:Atiyah_MacDonald}.
\end{proof}

\subsubsection{Finite presentability}
\label{sssec:finite_presentedness}

In this section we will introduce finite presentability of modules.

\begin{warning}
	In \cite{bog:Rolfsen}, Rolfsen uses \emph{row}-vectors while we use \emph{column}-vectors.
\end{warning}

\begin{notation}
	For a ring $R$ we denote by $\Mat_{R}(n,m)$ the set of $n\times m$-matrices over $R$.
\end{notation}

\begin{definition}
	Let $R$ be a ring and let $M$ be an $R$-module.
	\begin{liste}
		\item We call $M$ \emph{finitely generated} (over $R$) if there exists $n\in\N$ and an exact sequence of the form
			\begin{equation*}
				\begin{tikzcd}
					{R^n} & M & 0\point
					\arrow[from=1-1, to=1-2]
					\arrow[from=1-2, to=1-3]
				\end{tikzcd}
			\end{equation*}
		\item We call $M$ \emph{finitely presented} (over $R$) if there exists $m,n\in\N$ and an exact sequence of the form
			\begin{equation*}
				\begin{tikzcd}
					{R^m} & {R^n} & M & 0\point
					\arrow["p", from=1-1, to=1-2]
					\arrow[from=1-2, to=1-3]
					\arrow[from=1-3, to=1-4]
				\end{tikzcd}
			\end{equation*}
			In this case we call such a $p$ a \emph{presentation} of $M$.
			We will say that a matrix in $\Mat_{R}(n,m)$ a \emph{presents} $M$ if it represents a presentation of $M$.\qedhere
	\end{liste}
\end{definition}

\begin{notation}
	Let $P\in\Mat_{R}(n,m)$ represent $p\colon R^{m} \to R^{n}$.
	We will let $\coker P$ denote $\coker p$ (this does, of course, not depend on the representation of $P$ up to isomorphism).
\end{notation}

\begin{remark*}
	Note that any matrix $P$, presents $\coker P$ and that a matrix presents a module $M$, if and only if it presents all modules isomorphic to $M$.
	So a matrix $P$ presents $M$ if and only if $M\cong\coker P$.
\end{remark*}

\begin{remark*}
	For you category-theorists out there,\footnote{you know who you are.} an equivalent definition of finitely presented would be that $M$ is an ``($\omega$-)compact object'' in $\Mod_{R}$ or just a ``finitely presented object'' in $\Mod_{R}$ depending on name-convention.
\end{remark*}

\begin{lemma}\label{lem:noetherian_gives_finite_presentedness}
	A ring $R$ is Noetherian if and only if being a finitely presented module over $R$ is the same as being a finitely generated module over $R$.
\end{lemma}

\begin{proof}
	Being finitely presented implies being finitely generated over any ring since being finitely presented has a stricter requirement.
	We will first prove the ``if'' part of the statement.
	To do this it suffices to show that if $R$ is a ring and there exists an $R$-module $M$, which is finitely generated over $R$, but not finitely presented over $R$, then $R$ is not Noetherian.
	Fix such a finitely generated $R$-module $M$ and fix a surjective map $f\colon R^{n} \epi M$.
	Since $M$ is not finitely presented, the sub-($R$-)module $\ker f$ of $R^{n}$ cannot be finitely generated (as we will see in more detail in the proof of the ``only if'' part of the statement).
	But this means, that $R$ cannot be Noetherian (because if $R$ is Noetherian it follows from \cite[corollary 6.4]{bog:Atiyah_MacDonald} that $R^{n}$ is Noetherian and by \cite[prop. 6.2]{bog:Atiyah_MacDonald} that $\ker f$ is finitely generated over $R$).

	We will now prove the ``only if'' part of the statement.
	To do this it suffices to show, that if $R$ is Noetherian then being finitely generated over $R$ implies being finitely presented over $R$.
	Assume $R$ is Noetherian and let $M$ be a finitely generated $R$-module.
	Since $M$ is finitely generated we can fix a surjection $f\colon R^{n} \epi M$ for some $n\in\N$.
	The identity $\id\colon R^{n} \to R^{n}$ gives us that $R^{n}$ is a finitely generated $R$-module so, since $R$ is Noetherian, it follows from \cite[prop. 6.5]{bog:Atiyah_MacDonald} that $R^{n}$ is a Noetherian module.
	Because $\ker f$ forms a submodule of $R^{n}$ it now follows from \cite[prop. 6.2]{bog:Atiyah_MacDonald} that $\ker f$ is finitely generated.
	Because $\ker f$ is finitely generated we can fix $p\colon R^{m} \epi\ker f$ for some $m\in\N$.
	Extending the codomain of $p$ to $R^{n}$ gives us the exact sequence
	\[\begin{tikzcd}
		{R^m} & {R^n} & M & 0,
		\arrow["p", from=1-1, to=1-2]
		\arrow["f", from=1-2, to=1-3]
		\arrow[from=1-3, to=1-4]
	\end{tikzcd}\]
	so $M$ is finitely presented.
\end{proof}

\subsubsection{The order ideal}
\label{sssec:the_order_ideal}

In this section we will define the \emph{order ideal}, also called the \emph{(level 0) Fitting ideal}, of a finitely presented module. 

\begin{construction}
	Let $M$ be a finitely presented $R$-module and $P\in\Mat_{R}(n,m)$ be a matrix presenting $M$.
	We define the \emph{order ideal} of $P$, which we will denote by $\order{P}$, to be the ideal in $R$ generated by the $n\times n$-minors of $P$ with the convention, that if $m<n$ then $\order{P}=(0)$.
\end{construction}

\begin{theorem}\label{thm:well_definedness_order_ideal}
	Two matrices $P,Q$ over $R$ present isomorphic $R$-modules if and only if it is possible to get from one to the other by applying the following operations (also found on \cite[204]{bog:Rolfsen} - with row-vector convention) finitely many times.
	Furthermore, the order ideal of a matrix is unchanged by each of these operations.
	\begin{nlist}
		\item\label{it:order_ideal_1} Interchange two rows or columns.
		\item\label{it:order_ideal_2} Add to any row an $R$-linear combination of other rows.
		\item\label{it:order_ideal_3} Add to any column an $R$-linear combination of other columns.
		\item\label{it:order_ideal_4} Multiply a row or column by a unit of $R$.
		\item\label{it:order_ideal_5} Replace $P$ with the matrix 
			\begin{equation*}
				\left[
					\begin{matrix}
						1     & 0                   & 0    &\cdots\\
						\cline{2-4}
						r_{1} &\multicolumn{1}{|c}{}&       &\\
						r_{2} &\multicolumn{1}{|c}{}& P     &\\
						\vdots&\multicolumn{1}{|c}{}&       &
					\end{matrix}
					\hspace{4pt}
				\right]
			\end{equation*}
			with $r_{i}\in R$.
		\item\label{it:order_ideal_6} The reverse of \ref{it:order_ideal_5}.
		\item\label{it:order_ideal_7} Adjoin a new column which is an $R$-linear combination of columns of $P$.
		\item\label{it:order_ideal_8} Delete a column which is an $R$-linear combination of columns of $P$.
	\end{nlist}
\end{theorem}

We apologise sincerely, dear reader; some parts of the following proof is horribly written.
We recommend the reader proof this result themselves as this is (probably) easier and more enlightening (and less painful).
Otherwise see \cite[Section 07Z6]{url:stacks-project} for another proof.

\begin{proof}
	We begin by proving the ``if'' part of the statement, i.e.\ that applying each of the operations does not change $\coker P$ up to isomorphism.
	The operations \ref{it:order_ideal_1}--\ref{it:order_ideal_4} correspond to change of basis, which does not change $\coker P$ up to isomorphism.

	We will now prove that operation \ref{it:order_ideal_5} (and thereby also \ref{it:order_ideal_6}) does not change $\coker P$ up to isomorphism.
	Let 
	\begin{equation*}
		P' = 
		\begin{bmatrix}
			1 & 0 \\
			r & P 
		\end{bmatrix}
	\end{equation*}
	be the result of applying \ref{it:order_ideal_5} to $P$.
	Define $d_{0}\colon R^{n+1} \to R^{n}$ by 
	\begin{equation*}
		\begin{bmatrix}
			 x_{0}\\
			 \vdots\\
			 x_{n}
		\end{bmatrix}
		\xmapsto{d_{0}}
		\begin{bmatrix}
			x_{1}\\
			\vdots\\
			x_{n}
		\end{bmatrix}
		- x_{0}r,\qquad \text{for all }
		\begin{bmatrix}
			 x_{0}\\
			 \vdots\\
			 x_{n}
		\end{bmatrix}
		\in R^{n+1}\point
	\end{equation*}
	We see that $x\in\im P'$ implies $d_{0}(x) \in\im P$ for all $x\in R^{n+1}$, so we can define $\phi\colon \coker P'\to\coker P$ as the unique map making the following diagram commute:
	\begin{equation}\label{eq:cokernel_diagram}
		\begin{tikzcd}
			{R^{n+1}} & {R^n} \\
			{\coker P'} & {\coker P}\comma
			\arrow["{d_0}", from=1-1, to=1-2]
			\arrow[from=1-1, to=2-1]
			\arrow[from=1-2, to=2-2]
			\arrow["{\exists!\phi}"', from=2-1, to=2-2]
		\end{tikzcd}
	\end{equation}
	where the unnamed maps are the projections.
	We will now show, that $\phi$ is an isomorphism.
	The projections and $d_{0}$ in \eqref{eq:cokernel_diagram} is surjective, so $\phi$ must be surjective as well.
	We will now show that $\phi$ is injective, which is equivalent to $\ker\phi=0$, which is equivalent to:
	If $x=[x_{0},\ldots ,x_{n}]^{\transpose}\in R^{n+1}$ such that $d_{0}(x)\in\im P$ then $x\in\im P'$.
	Let $x=[x_{0},\ldots ,x_{n}]^{\transpose}\in R^{n+1}$ such that $d_{0}(x)\in\im P$.
	Fix $y=[ y_{1},\ldots ,y_{m}]^{\transpose}\in R^{m}$ such that $Py=d_{0}(x)= x'- x_{0}r$.
	Let $x'$ denote $[ x_{1},\ldots ,x_{n}]^{\transpose}$ and $y'$ denote $[ x_{0},y_{1},\ldots ,y_{m}]^{\transpose}\in R^{m+1}$.
	We now see that 
	\begin{equation*}
		P'y' = 
		\begin{bmatrix}
			1 & 0 \\
			r & P
		\end{bmatrix}
		\begin{bmatrix}
			 x_{0}\\
			 y
		\end{bmatrix}
		= x_{0}
		\begin{bmatrix}
			1 \\
			r
		\end{bmatrix}
		+
		\begin{bmatrix}
			0 \\
			Py
		\end{bmatrix}
		=
		\begin{bmatrix}
			 x_{0}\\
			 x_{0} r
		\end{bmatrix}
		+
		\begin{bmatrix}
			0 \\
			x' - x_{0}r
		\end{bmatrix}
		=
		\begin{bmatrix}
			 x_{0}\\
			 x'
		\end{bmatrix}
		= x\comma
	\end{equation*}
	so $x\in\im P'$ and we are done.

	We will now prove that operation \ref{it:order_ideal_7} (and thereby also \ref{it:order_ideal_8}) does not change $\coker P$ up to isomorphism.
	We have $\cod P'=\cod P$ and $\im P'=\im P$ (since the image is the span of the columns), so we have $\coker P'=\coker P$.

	Now, we prove the ``only if'' part of the statement.
	Let $P$ and $Q$ be matrices over $R$ presenting isomorphic $R$-modules $M$ and $N$ respectively.
	We now want to show that it is possible to get from $P$ to $Q$ using the operations finitely many times.
	We will do this by constructing a larger matrix $S$ from $P$ and $Q$ and showing, that we can get $P$ \emph{and} $Q$ from $S$ by finitely many applications of the operations.
	Fix an isomorphism $\phi\colon M\to N$.
	We note that the matrix 
	\begin{equation*}
	\begin{bmatrix}
		P & 0 \\
		0 & Q
	\end{bmatrix}
	\end{equation*}
	presents $M\oplus N$, so adding the relations $(m,\phi(m))=0$ for all $m\in M$ will give us a presentation of $M$ (and, of course $N$).
	We can also add the \emph{redundant} and equivalent relations $(\phi^{-1}(n),n)=0$ for all $n\in N$, making our construction symmetric in $P$ and $Q$.
	Let $g_{1},\ldots ,g_{n}$ and $g'_{1},\ldots g'_{n'}$ denote the generators of $M$ and $N$ with respect to $P$ and $Q$ respectively.
	Since the $g_{i}$ generate $M$ and the $g'_{i}$ generate $N$, we can write $\phi(g_{i})= \sum_{j}c_{i,j}g'_{j}$ and $\phi^{-1}(g'_{i})=\sum_{j} c'_{i,j}g_{j}$ and define $C\coloneqq [c_{i,j}]$ and $C'\coloneqq [c'_{i,j}]$.
	We can therefore add the relators $(g_{i},\phi(g_{i}))$ and $(\phi^{-1}(g'_{i}),g'_{i})$ by defining
	\begin{equation*}
		S\coloneqq 
		\begin{bmatrix}
			P & 0 & I & C'\\
			0 & Q & C & I
		\end{bmatrix}
		\point
	\end{equation*}
	It now suffices to show, that we can get from $S$ to both $P$ and $Q$ using the operations finitely many times, which is what we will do.
	Because of our added redundancy, the argument we give will be symmetric in $P$ and $Q$, so we will only state how to get from $S$ to $Q$.
	The redundancy allows us to use \ref{it:order_ideal_8} to remove $[\begin{smallmatrix} C' \\ I \end{smallmatrix}]$ and get: 
	\begin{align*}
		\mathllap{S\coloneqq }
		\begin{bmatrix}
			P & 0 & I & C'\\
			0 & Q & C & I
		\end{bmatrix}
		&\stackrel{\ref{it:order_ideal_8}}{\leadsto}
		\begin{bmatrix}
			P & 0 & I \\
			0 & Q & C
		\end{bmatrix}\\
		  &\stackrel{\ref{it:order_ideal_3}}{\leadsto}
		\begin{bmatrix}
			0   & 0 & I \\
			-CP & Q & C
		\end{bmatrix}\\
		  &\stackrel{\ref{it:order_ideal_1}}{\leadsto}
		\begin{bmatrix}
			I & 0 & 0   \\
			C & Q & -CP
		\end{bmatrix}\\
		  &\stackrel{\ref{it:order_ideal_6}}{\leadsto}
		\begin{bmatrix}
			Q & -CP
		\end{bmatrix}\\
		  &\stackrel{\ref{it:order_ideal_8}}{\leadsto}
		Q\comma
	\end{align*}
	where we, in the last step, use that $\im CP\subset \im Q$, which follows from commutativity and exactness of the rows of:
	\begin{equation*}
		\begin{tikzcd}
			{R^m} & {R^n} & {M} & 0 \\
			{R^{m'}} & {R^{n'}} & {N} & 0\point
			\arrow["P", from=1-1, to=1-2]
			\arrow[from=1-2, to=1-3]
			\arrow[from=1-3, to=1-4]
			\arrow["Q", from=2-1, to=2-2]
			\arrow[from=2-2, to=2-3]
			\arrow[from=2-3, to=2-4]
			\arrow["\phi", from=1-3, to=2-3]
			\arrow["C", from=1-2, to=2-2]
		\end{tikzcd}
	\end{equation*}
	This concludes the proof of the ``only if'' part of the statement.

	We will now prove, that $\order{P}$ is unchanged by the operations for $P\in\Mat_{R}(n,m)$.
	In the case $m<n$ this is trivial for all the operations except \ref{it:order_ideal_7} and \ref{it:order_ideal_8}, so we will consider these cases as treated.
	Applying \ref{it:order_ideal_1} to $P$ can only change the signs of the $n\times n$ minors of $P$ and does thereby not change the ideal they generate, $\order{P}$.

	Denote by $[a]$ the set $\{1,\ldots ,a\}$ with the well-ordering from $\N$ for all $a\in\N$ and let $\simp_{+}(a,b)$ denote the set of strictly increasing functions $[a]\to [b]$ for all $a,b\in\N$.
	For a matrix $X=[x_{1},\ldots,x_{m}]\in\Mat_{R}(n,m)$ denote by $X_{\tau}$ the $n\times n$ submatrix $[x_{\tau(1)},\ldots ,x_{\tau(n)}]\in\Mat_{R}(n,n)$ of $X$ and denote by $\mathfrak{m}_{\tau}$ the determinant $\det P_{\tau}$ for all $\tau\in\simp_{+}(n,m)$.
	Clearly $\tau\mapsto X_{\tau}$ is a bijection from $\simp_{+}(n,m)$ to the set of $n\times n$ submatrices of $X$ for any $X\in\Mat_{R}(n,m)$.
	Therefore $\order{P}=(\{\mathfrak{m}_{\tau}\pipe \tau\in\simp_{+}(n,m)\})$.

	We will now prove that \ref{it:order_ideal_3} does not change $\order{P}$.
	Let $P\eqqcolon[p_{1},\ldots ,p_{m}]$.
	Now, fix an $i$ and apply \ref{it:order_ideal_3} to $P$ at the $i^{\text{th}}$ column to get $P'\coloneqq [p_{1},\ldots ,p_{i} +\sum_{j\neq i} a_{j}p_{j},\ldots ,p_{m}]$.
	We now want to show, that $\order{P}=\order{P'}$.
	Let $\sigma\in\simp_{+}(n,m)$.
	If $i\notin\im\sigma$ we have $P'_{\sigma}=P_{\sigma}$ and so $\det P'_{\sigma}=\mathfrak{m}_{\sigma}$.
	If $i\in\im\sigma$ there exists a (unique) $k\in[n]$ such that $\sigma (k)=i$ and then, letting $P'\eqqcolon[p'_{1},\ldots ,p'_{m}]$, we see that
	\begin{align*}
		\det P'_{\sigma} &= \det [p'_{ \sigma(1)},\ldots ,p'_{\sigma (k)} ,\ldots ,p'_{ \sigma (m)}]\\
						 &= \det [p_{ \sigma(1)},\ldots ,p_{i} +\sum_{j\neq i} a_{j} p_{j},\ldots ,p_{ \sigma (m)}]\\
						 &= \det [p_{\sigma(1)},\ldots ,p_{\sigma (m)}]+\sum_{j\neq i} a_{j} \det[p_{\sigma(1)},\ldots,p_{j},\ldots,p_{\sigma(m)}]\\
						 &= \mathfrak{m}_{\sigma} +\sum_{j\neq i} a_{j} \det[p_{\sigma(1)},\ldots,p_{j},\ldots,p_{\sigma(n)}]\\
						 &= \mathfrak{m}_{\sigma} +\sum_{j\neq i} a'_{j} \mathfrak{m}_{\sigma_{j}},
	\end{align*}
	where $\sigma_{j}$ is the unique element of $\simp_{+}(n,m)$ such that $\im \sigma_{j}=(\im \sigma \uden \{i\})\cup\{j\}$ (so, in particular $i\notin\im\sigma_{j}$ for $j\neq i$) and $a_{j}'$ is $a_{j}$ if $\sigma_{j}(k)=j$ and $-a_{j}$ otherwise for all $j\neq i$.
	Now, it follows, that $\order{P'}=\order{P}$ because the generators of $\order{P'}$ have only been changed in the following way, which does not change the ideal, they generate:
	We have added $R$-linear combinations of $\mathfrak{m}_{\tau}$'s with $i\notin\im\tau$ to $\mathfrak{m}_{\sigma}$'s with $i\in\im \sigma$.

	Applying \ref{it:order_ideal_2} or \ref{it:order_ideal_4} to $P$ either does nothing or the same to every $n\times n$ submatrix of $P$ so, since the determinant is independent (up to $R$-associatedness) of these, the order ideal is unchanged by these.

	We will now prove, that operation \ref{it:order_ideal_5} does not change $\order{P}$.
	Again, apply \ref{it:order_ideal_5} to $P$ and denote the resulting $(n+1)\times(m+1)$ matrix by $P'$.
	The top row of any $(n+1)\times(n+1)$ submatrix of $P'$ not containing the left-most column of $P'$ is identically $0$ and so the determinant of such a submatrix is $0$.
	Therefore we can disregard these submatrices (as they do not contribute to $\order{P'}$) and they are exactly the $P'_{\tau}$ with $1\notin\im\tau$.
	The rest are exactly the $P'_{\tau}$ with $\tau\in\simp_{+}(n+1,m+1)$ in the image of the function $s\colon \simp_{+}(n,m) \to\simp_{+}(n+1,m+1)$ defined by 
	\begin{equation*}
		s(\sigma) (i) =
		\begin{cases}
			1 & i=1\\
			\sigma(i-1)+1 & \text{otherwise}
		\end{cases}
	\end{equation*}
	for all $\sigma \in\simp_{+}(n,m)$.
	Now, by Laplace expansion, we have $\det P'_{s(\tau)}=\det P_{\tau}=\mathfrak{m}_{\tau}$ so the operation has only added minors equal to $0$ and hence not changed $\order{P}$.

	The proof, that operation \ref{it:order_ideal_7} does not change $\order{P}$ is similar enough to \ref{it:order_ideal_3} that we will omit it (though one has to check it in the case $n<m$).

	That $\order{P}$ is independent of \ref{it:order_ideal_8} and \ref{it:order_ideal_6} follows by symmetry of independence of \ref{it:order_ideal_7} and \ref{it:order_ideal_5} respectively.
\end{proof}

\begin{corollary}
	Let $M$ be a finitely presented $R$-module then the order ideal, $\order{P}$, in $R$ is independent of both the choice of matrix-representation, $P$, of $p$ and the choice of presentation, $p$, of $M$.
\end{corollary}

In light of this theorem and corollary we can now define the order ideal of $M$ for any finitely presented module $M$.

\begin{definition}
	Let $M$ be a finitely presented $R$-module.
	The \emph{order ideal}, denoted $\order{M}$, of $M$ is the order ideal of any matrix presenting $M$.
\end{definition}

\begin{remark*}
	Note, that if there exists a square matrix presenting $M$, then the order ideal of $M$ is principal.
\end{remark*}

\subsection{Construction of the Alexander Polynomial}

It turns out, that $\A(K)$ is a finitely presented $\Lambda$-module and that its order ideal is principal for all (smooth) knots $K\colon S^{1} \mono S^{3}$.
We want to define the \emph{Alexander polynomial} of a knot $K$ as a generator of the order ideal of $\A(K)$.
This is what we will do in this section.
First, we will show, abstractly, that $\A(K)$ is finitely presented and then we will construct a square matrix presenting $\A(K)$.

\subsubsection{Abstract finite presentability argument}
\label{sssec:Abstract_finite_presentedness_argument}

In this section we will show, abstractly, that $\A(K)$ is finitely presented.
In \cref{sssec:construction_of_the_alexander_polynomial} we construct a presentation of $\A(K)$ (not using the results of this section).

We can lift $\Delta$-complex structures along coverings, i.e.:

\begin{construction}\label{con:lift_of_delta_complex}
	Let $p\colon \tilde{X} \to X$ be a path-connected covering and let $X_{\bullet}$ be a $\Delta$-complex structure on $X$.
	Let $\sigma\colon \Delta^{n} \mono X$ be an element of $X_{n}$.
	Now (since $\Delta^{n}$ is contractible) it follows from \cite[theorem 2.7.2]{bog:AlgTopI}\footnote{Since $X$ has a $\Delta$-complex structure it is locally path-connected and therefore, since $p\colon \tilde{X} \to X$ is a covering, so is $\tilde{X}$.}
	that $\sigma$ lifts, uniquely, to a map $\tilde{\sigma}_{x}\colon \Delta^{n} \mono X$ along $p$ for each $x\in \fib(p)$.
	Define $\tilde{X}_{\bullet}$ by $\tilde{X}_{n} \coloneqq \{ \tilde{\sigma}_{x} \pipe \sigma\in X_{n} , x\in\fib(p)\}$.
\end{construction}

\begin{lemma}\label{lem:lift_of_delta_complex}
	In \cref{con:lift_of_delta_complex}, $\tilde{X}_{\bullet}$ is a $\Delta$-complex structure on $\tilde{X}$.
\end{lemma}

\begin{proof}
	Readily follows.
\end{proof}

\begin{lemma}
	The $\Lambda$-module $\A(K)$ is finitely presented over $\Lambda$.
\end{lemma}

\begin{proof}
	Since $X_{K}$ is a closed, compact manifold with boundary, it has the structure of a finite $\Delta$-complex.
	Fix a finite $\Delta$-complex structure on $X_{K}$ and call it $X_{\bullet}$.
	By using \cref{lem:lift_of_delta_complex}, we can now lift $X_{\bullet}$ to a $\Delta$-complex structure $\tilde{X}_{\bullet}$ on $\tilde{X}_{K}$ along the covering $p\colon \tilde{X}_{K} \to X_{K}$.
	The group $\Deck(p)\cong\Z$ acts on $\tilde{X}_{\bullet}$ by post-composition which gives an action of $\Deck(p)$ on $H_{1}^{\Delta}(\tilde{X})$.
	Under the isomorphism $H^{\Delta}_{1}(\tilde{X}) \cong H_{1}(\tilde{X};\Z)$ induced by the inclusion $\tilde{X}_{\bullet}\mono \Sing_{\bullet}^{\text{nd}} (\tilde{X})$ these actions of $\Deck(p)$ (the other one being the one from $\A(K)$) agree.
	This means, $\Delta_{1}(\tilde{X}_{K})\cong \Delta_{1}(X_{K}) \otimes \Lambda$ and so $\Delta_{1}(\tilde{X}_{K})$ is free of finite rank over $\Lambda$ (Which is Noetherian by \cref{lem:lambda_is_noetherian})  so $\Delta_{1}(\tilde{X}_{K})$ is Noetherian over $\Lambda$ and therefore the (simplicial) $1$-cycles and $1$-boundaries are as well and then it follows from \cite[prop. 6.3]{bog:Atiyah_MacDonald} that $\A(K)$ is Noetherian and hence finitely generated over $\Lambda$ as well.
	Since $\Lambda$ is Noetherian we are done by \cref{lem:noetherian_gives_finite_presentedness}.
\end{proof}

\subsubsection{Construction of the Alexander polynomial}
\label{sssec:construction_of_the_alexander_polynomial}

To show that $\order{\A(K)}$ is principal we will describe the generators and relations of $\A(K)$ by giving a matrix presenting $\A(K)$.
The matrix in question is $V-tV^{\transpose}$ for a Seifert matrix $V$, where we think of the matrix $V$ as a matrix over $\Lambda$ by extension of scalars.

Now, for the main result of this project:

\begin{theorem}
	Let $K\colon S^{1} \mono S^{3}$ be a knot and $V$ a Seifert matrix for $K$, then $V^{\transpose} -tV$ presents $\A(K)$ over $\Lambda$.
\end{theorem}

To prove this theorem we will use the Mayer--Vietoris sequence twice, first on $X_{K}$ and then (using the results of the first application) on $\tilde{X}_{K}$.
From this, we will extract a presentation of $\A(K)$, which we will then show gives us that $V-tV^{\transpose}$ presents $\A(K)$.

\begin{proof}
	In this proof we will only use homology with integral coefficients, therefore we will suppress the coefficients from the notation.

	First, we construct the two subspaces of $X_{K}$ we will use the Mayer--Vietoris sequence on.
	Choose a tubular neighbourhood $\nu(K)$ of $K$ then choose a Seifert surface $\Sigma_{g}$ of $K$, which is ``trivial'' in $\nu(K)$.
	Now, choose a bicollar $b$ of $\Sigma_{g}$ such that $b(K\times [-1,1])\subset \interior{\nu}(K)$.
	As always, $X_{K}$ denotes $S^{3}\uden\interior{\nu}(K)$.
	Define 
	\begin{equation*}
		\bm{\Sigma}\coloneqq b(\Sigma_{g}\times[-1,1])\cap X_{K} \quad\text{and}\quad X_{\bm{\Sigma}} \coloneqq X_{K} \uden \interior{b}(\Sigma_{g}\times[- 1/2, 1/2])\point
	\end{equation*}
	These will be the subspaces we will use Mayer--Vietoris on.
	Define 
	\begin{equation*}
		\bm{\Sigma}^{+}\coloneqq b(\Sigma_{g}\times[1/2,1])\cap X_{K} \quad\text{and}\quad \bm{\Sigma}^{-}\coloneqq b(\Sigma_{g}\times[-1,-1/2])\cap X_{K}\point
	\end{equation*}
	Note that $\bm{\Sigma}\cap X_{\bm{\Sigma}}= \bm{\Sigma}^{+}\cup \bm{\Sigma}^{-}$, the union being disjoint, and $\bm{\Sigma}^{+} \simeq \Sigma_{g}^{+}\simeq\Sigma_{g}^{-}\simeq \bm{\Sigma}^{-}$.

	Let $p\colon \tilde{X}_{K} \to X_{K}$ be the infinite cyclic covering and let $\tilde{\bm{\Sigma}}\coloneqq p^{-1}(\bm{\Sigma})$ and $\tilde{X}_{\bm{\Sigma}}\coloneqq p^{-1}(X_{\bm{\Sigma}})$.
	We can illustrate our current construction in the following diagram, labelling the inclusions:
	\begin{equation}\label{eq:diagram_of_the_covering}
		\begin{tikzcd}
			\tilde{\bm{\Sigma}} & {\tilde{X}_K} & \tilde{X}_{\bm{\Sigma}} \\
			{\bm{\Sigma}} & {X_K} & {X_{\bm{\Sigma}}\point}
			\arrow[from=1-3, to=2-3]
			\arrow[from=1-1, to=2-1]
			\arrow["{p}", from=1-2, to=2-2]
			\arrow["{\tilde{\iota}^{1}}", hook, from=1-1, to=1-2]
			\arrow["{\tilde{\iota}^{2}}"', hook', from=1-3, to=1-2]
			\arrow["{\iota^{1}}", hook, from=2-1, to=2-2]
			\arrow["{\iota^{2}}"', hook', from=2-3, to=2-2]
			\arrow["\ulcorner"{anchor=center, pos=0.125}, draw=none, from=1-1, to=2-2]
			\arrow["\urcorner"{anchor=center, pos=0.125}, draw=none, from=1-3, to=2-2]
		\end{tikzcd}
	\end{equation}
	We note that $\Int(\bm{\Sigma})\cup \Int(X_{\bm{\Sigma}}) = X_{K}$, so we can use the Mayer--Vietoris sequence on the bottom row of \eqref{eq:diagram_of_the_covering}, and get the exact sequence:
	\begin{equation}\label{eq:mayer-vietoris-bottom}
		\begin{tikzcd}
			{H_{1}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})} \arrow{r} & {H_{1}(\bm{\Sigma})\oplus H_{1}(X_{\bm{\Sigma}})}\arrow{r}
			\arrow[phantom]{d}[name=X, anchor=center]{}
																	 & {H_{1}(X_{K})} \arrow[rounded corners, to path={ -- ([xshift=2ex]\tikztostart.east)
																		 |- (X.center) \tikztonodes
																		 -| ([xshift=-2ex]\tikztotarget.west)
																	 -- (\tikztotarget)}]{dll}[at end,description]{\delta} \\      
			{H_{0}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})\point} & {\phantom{H_{1}}}
		\end{tikzcd}
	\end{equation}
	
	We will now show that $p$ is trivial over $\bm{\Sigma}$ and $X_{\bm{\Sigma}}$ using the following lemma:

	\begin{lemma}
		Let $A\subset X_{K}$ be a subspace.
		If the map $H_{1}(A) \to H_{1}(X_{K})$ induced by the inclusion is $0$ then $p$ is trivial over $A$.
	\end{lemma}
	
	\begin{proofoflemma}
		Let $i\colon A\mono X_{K}$ denote the inclusion.
		Using \cite[Theorem 2.7.2]{bog:AlgTopI}, we get that $p$ is trivial over $A$ if and only if $i_{*}\pi_{1}(A,a) \subset p_{*}\pi_{1}(\tilde{X}_{K},\tilde{a})$ for all $a\in A$ and all $\tilde{a}\in\fib_{p}(a)$.
		By definition of $p$ we have $p_{*}\pi_{1}(\tilde{X}_{K},\tilde{a})$ is the commutator subgroup of $\pi_{1}(X_{K},a)$ which is exactly $\ker(\hurewicz\colon \pi_{1}(X_{K},a) \to H_{1} (X_{K}))$.
		Therefore we have that $p$ is trivial over $A$ if and only if the composition
		\begin{equation*}
			\begin{tikzcd}
				{\pi_1(A,a)} & {\pi_1(X_K,a)} & {H_1(X_K)}
				\arrow["i_{*}", from=1-1, to=1-2]
				\arrow["\hurewicz", from=1-2, to=1-3]
			\end{tikzcd}
		\end{equation*}
		is $0$.
		We have the commutative square
		\begin{equation*}
			\begin{tikzcd}
				{\pi_1(A,a)} & {\pi_1(X_K,a)} \\
				{H_1(A)} & {H_1(X_K)\comma}
				\arrow["{i_*}", from=1-1, to=1-2]
				\arrow["\hurewicz"', from=1-1, to=2-1]
				\arrow["\hurewicz"', from=1-2, to=2-2]
				\arrow["{i_*}", from=2-1, to=2-2]
			\end{tikzcd}
		\end{equation*}
		from which it follows, that if $i_{*}\colon H_{1}(A) \to H_{1}(X_{K})$ is $0$, then 
		\begin{equation*}
			\hurewicz\circ i_{*} = i_{*} \circ\hurewicz = 0\circ \hurewicz =0
		\end{equation*}
		and so $p$ is trivial over $A$.
	\end{proofoflemma}

	To show, that $p$ is trivial over $\bm{\Sigma}$ and $X_{\bm{\Sigma}}$ it suffices (by the above lemma) to show that the maps induced on homology by the inclusions $\bm{\Sigma}\mono X_{K}$ and $X_{\bm{\Sigma}}\mono X_{K}$ are trivial.
	We will denote the inclusions as follows:
	\begin{equation*}
		\begin{tikzcd}
			\bm{\Sigma}^{+} \sqcup \bm{\Sigma}^{-} & {X_{\bm{\Sigma}}} \\
			{\bm{\Sigma}} & {X_K}
			\arrow["i^{1}"', hook, from=1-1, to=2-1]
			\arrow["i^{2}", hook, from=1-1, to=1-2]
			\arrow["\iota^{2}", hook, from=1-2, to=2-2]
			\arrow["\iota^{1}", hook, from=2-1, to=2-2]
		\end{tikzcd}
		\quad\text{and}\quad
		\begin{tikzcd}
			{\tilde{\bm{\Sigma}}^{+} \sqcup \tilde{\bm{\Sigma}}^{-}} & {\tilde{X}_{\bm{\Sigma}}} \\
			{\tilde{\bm{\Sigma}}} & {\tilde{X}_K\point}
			\arrow["{\tilde{i}^{1}}"', hook, from=1-1, to=2-1]
			\arrow["{\tilde{i}^{2}}", hook, from=1-1, to=1-2]
			\arrow["{\tilde{\iota}^{2}}", hook, from=1-2, to=2-2]
			\arrow["{\tilde{\iota}^{1}}", hook, from=2-1, to=2-2]
		\end{tikzcd}
	\end{equation*}

	The map $\iota^{1}_{*}\colon H_{1}(\bm{\Sigma})\to H_{1}(X_{K})$ is trivial for geometric reasons (none of the generators form a meridian of $K$), so $p$ is trivial over $\bm{\Sigma}$.

	To show that $\iota^{2}_{*}\colon H_{1}(X_{\bm{\Sigma}} )\to H_{1}(X_{K})$ is $0$, it now suffices to show that 
	\begin{equation*}
		\iota^{1}_{*} + \iota^{2}_{*}\colon H_{1}(\bm{\Sigma})\oplus H_{1}(X_{\bm{\Sigma}})\to H_{1}(X_{K})
	\end{equation*}
	is $0$, and (since this is the map in \eqref{eq:mayer-vietoris-bottom}) showing this is $0$ is equivalent to showing that $\delta$ in \eqref{eq:mayer-vietoris-bottom} is injective (by exactness).
	To show that $\delta$ in \eqref{eq:mayer-vietoris-bottom} is injective we will ``calculate'' it.
	We have that $H_{1}(X_{K}) \cong \Z$ generated by the meridian and that $H_{0}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})\cong H_{0}(\bm{\Sigma}^{+})\oplus H_{0}(\bm{\Sigma}^{-})\cong \Z^{2}$.
	Define $\mathcal{U}\coloneqq\{\bm{\Sigma},X_{\bm{\Sigma}}\}$.
	Recall that $\delta$ is a connecting homomorphism in homology of the short exact sequence
	\begin{equation*}
			0 \to C_*(\bm{\Sigma}\cap X_{\bm{\Sigma}}) \to C_*(\bm{\Sigma})\oplus C_*(X_{\bm{\Sigma}}) \to C_*^{\mathcal{U}}(X_K) \to 0
	\end{equation*}
	of chain complexes.
	To calculate $\delta$ we simply need to calculate where it sends $1$ i.e.\ the meridian (up to orientation/sign).
	Pick a meridian $m\in H_{1}(X_{K})$.\footnote{We can pick a specific model for the meridian inside a tubular neighbourhood of $K$ allowing us to control the situation completely.}
	Define $\sigma_{1}\colon \Delta^{1} \to \Int(\bm{\Sigma})$ and $\sigma_{2}\colon \Delta^{1} \to \Int(X_{\bm{\Sigma}})$ as in \cref{fig:connecting_homomorphism}.
	\begin{figure}[h]\centering
		\caption{Cross-section of the tubular neighbourhood of $K$. The grey area shows the intersection $\bm{\Sigma}\cap X_{\bm{\Sigma}}$.}\label{fig:connecting_homomorphism}
		\includegraphics[width=.7\textwidth,height=.6\textwidth,keepaspectratio]{Metapost/figures/connecting-homomorphism.mps}
	\end{figure}
	Now, $\sigma_{1} +\sigma_{2}$ is a $\mathcal{U}$-small $1$-chain and $(\iota^{1}_{*} + \iota^{2}_{*} )(\sigma_{1},\sigma_{2}) = m$.
	Therefore, by definition of the connecting homomorphism in homology, the meridian is sent to the (unique) homology class of the $0$-cycle whose image under $(i^{1}_{*},-i^{2}_{*})$ is $\partial(\sigma_{1},\sigma_{2})$ in $C_{1}(\bm{\Sigma})\oplus C_{1}(X_{\bm{\Sigma}})$.
	We see that $\partial(\sigma_{1},\sigma_{2})=(y-x,x-y)= (i^{1}_{*},-i^{2}_{*})(y-x)$ and so $\delta([m])=[x-y]$.
	This means $\delta\colon H_{1}(X_{K}) \to H_{0}(\bm{\Sigma}^{+} \sqcup \bm{\Sigma}^{-})$ corresponds to the map $[1,-1]^{\transpose}\colon \Z \to \Z^{2}$, which is injective, so $\delta$ is injective.

	Since the inclusions in the top row of \eqref{eq:diagram_of_the_covering} commute with the deck transformations of $p$, the Mayer--Vietoris sequence of the top row of \eqref{eq:diagram_of_the_covering} gives us a long exact sequence of $\Lambda$-modules.
	Since $p$ is trivial over $\bm{\Sigma}$ and $X_{\bm{\Sigma}}$ we get that $H_{n}(\tilde{\bm{\Sigma}})\cong H_{n}(\bm{\Sigma})[t^{\pm1}]$ and $H_{n}(\tilde{X}_{\bm{\Sigma}})\cong H_{n}(X_{\bm{\Sigma}})[t^{\pm1}]$ as $\Lambda$-modules for all $n\in\Z$.
	Therefore, the Mayer--Vietoris sequence of the top row of \eqref{eq:diagram_of_the_covering} gives us the exact sequence:
	\begin{equation}\label{eq:mayer-vietoris-top}
		\begin{tikzcd}
		{H_{1}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})[t^{\pm1}]} \arrow{r}{(\tilde{i}^{1}_{*},-\tilde{i}^{2}_{*})} & {H_{1}(\bm{\Sigma})[t^{\pm1}]\oplus H_{1}(X_{\bm{\Sigma}})[t^{\pm1}]}\arrow{r}{\tilde{\iota}^{1}_{*} + \tilde{\iota}^{2}_{*}}
			\arrow[phantom]{d}[name=X, anchor=center]{}
			& {H_{1}(\tilde{X}_{K})} \arrow[rounded corners, to path={ -- ([xshift=2ex]\tikztostart.east)
																		 |- (X.center) \tikztonodes
																		 -| ([xshift=-2ex]\tikztotarget.west)
																	 -- (\tikztotarget)}]{dll}[at end,description]{\tilde{\delta}} \\      
			{H_{0}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})[t^{\pm1}]} \arrow{r}{f} & {H_{0}(\bm{\Sigma})[t^{\pm1}]\oplus H_{0}(X_{\bm{\Sigma}} )[t^{\pm1}]} %\arrow{r} & {H_{n-1}(F)}
		\end{tikzcd}
	\end{equation}
	of $\Lambda$-modules.

	We will first show that $\tilde{\delta}$ in \eqref{eq:mayer-vietoris-top} is $0$.
	We will do this by showing, that $f$ is injective which we will do by ``calculating'' it.
	We have that 
	\begin{equation*}
		H_{0}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})[t^{\pm1}]\cong (\Z \oplus \Z)[t^{\pm1}]\cong \Lambda \oplus \Lambda
	\end{equation*}
	and, similarly, that 
	\begin{equation*}
		H_{0}(\bm{\Sigma})[t^{\pm1}]\oplus H_{0}(X_{\bm{\Sigma}})[t^{\pm1}] \cong \Lambda \oplus \Lambda\point
	\end{equation*}
	Choose a point $x\in p^{-1}(\bm{\Sigma}^{+})$.
	Then $[x]$ generates $H_{0}(\bm{\Sigma})[t^{\pm1}]$ and $H_{0}(X_{\bm{\Sigma}})[t^{\pm1}]$ and we can define isomorphisms $\phi\colon H_{0}(\bm{\Sigma})[t^{\pm1}]\to\Lambda$ and $\phi'\colon H_{0}(X_{\bm{\Sigma}})\to\Lambda$ by $[x]\mapsto1$.
	Similarly we can choose a point $y\in p^{-1}(\bm{\Sigma}^{-})$ such that $x$ and $y$ are in the same path component (lift) of $\tilde{\bm{\Sigma}}$.
	Now, by the above defined isomorphism we have $\phi([y])=1$.
	We know that $[y]$ also generates $H_{0}(X_{\bm{\Sigma}})[t^{\pm1}]$ and so $\phi'([y])\in\Lambda^{\times}$ i.e.\ an element of the form $\pm t^{n}$ with $n\in\Z$.
	If $\phi'([y])=\pm t^{n}$ with $n\neq \pm1$ then $\tilde{X}_{K}$ is not path-connected, which contradictions $p$ being the infinite cyclic covering.
	Therefore we can assume, WLOG, that $\phi'([y])=t$.
	We have now calculated $f$, it is the unique map making
	\begin{equation*}
		\begin{tikzcd}[ampersand replacement=\&]
			{H_0(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})[t^{\pm1}]} \& {H_{0}(\bm{\Sigma})[t^{\pm1}]\oplus H_{0}(X_{\bm{\Sigma}})[t^{\pm1}]} \\
			\Lambda\oplus\Lambda \& \Lambda\oplus\Lambda
			\arrow["{\left[\begin{smallmatrix} 1 & 1 \\ 1 & t\end{smallmatrix}\right]}", from=2-1, to=2-2]
			\arrow["\psi"', from=1-1, to=2-1]
			\arrow["{(\phi,\phi')}", from=1-2, to=2-2]
			\arrow["f", from=1-1, to=1-2]
		\end{tikzcd}
	\end{equation*}
	commute, where $\psi$ is the isomorphism defined by $[x]\mapsto [1,0]^{\transpose}$ and $[y]\mapsto [0,1]^{\transpose}$.
	From this it is clear, that $f$ is injective.

	This means, we can extract the following exact sequence from \eqref{eq:mayer-vietoris-top}: 
	\begin{equation*}
		H_{1}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})[t^{\pm1}] \to H_{1}(\bm{\Sigma})[t^{\pm1}]\oplus H_{1}(X_{\bm{\Sigma}})[t^{\pm1}]\to H_{1}(\tilde{X}_{K})\to 0\comma
	\end{equation*}
	so since $H_{1}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})[t^{\pm1}]\cong H_{1}(\bm{\Sigma})[t^{\pm1}]\oplus H_{1}(X_{\bm{\Sigma}})[t^{\pm1}]\cong\Lambda^{4g}$ and $H_{1}(\tilde{X}_{K}) =\A(K)$ we get a presentation of $\A(K)$.
	We will now calculate this presentation
	\begin{equation*}
		H_{1}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})[t^{\pm1}]\to H_{1}(\bm{\Sigma})[t^{\pm1}]\oplus H_{1}(X_{\bm{\Sigma}})[t^{\pm1}]
	\end{equation*}
	of $\A(K)$ in \eqref{eq:mayer-vietoris-top} by calculating the corresponding map in \eqref{eq:mayer-vietoris-bottom} and use the same argument as when calculating $f$ above to calculate the lift.
	Consider 
	\begin{equation*}
		H_{1}(\bm{\Sigma}^{+}\sqcup \bm{\Sigma}^{-})\to H_{1}(\bm{\Sigma})\oplus H_{1}(X_{\bm{\Sigma}})\point
	\end{equation*}
	Pick a basis $([b_{i}])$ for $H_{1}(\Sigma_{g})\cong H_{1}(\bm{\Sigma})$ and assume WLOG that $V$ is a Seifert matrix for $K$ with respect to $([b_{i}])$ i.e.\ $v_{i,j}=\linking(b_{i},b^{+}_{j})$.
	Use $([b^{+}_{i}])$ and $([b^{-}_{i}])$ as bases for $H_{1}(\bm{\Sigma}^{+})$ and $H_{1}(\bm{\Sigma}^{-})$ respectively.
	With our chosen bases the map
	\begin{equation*}
		 H_{1}(\bm{\Sigma}^{+})\oplus H_{1}(\bm{\Sigma}^{-})\to H_{1}(\bm{\Sigma})
	\end{equation*}
	is represented by $\begin{bmatrix} I & I\end{bmatrix}$.
	Using Alexander duality, and \cref{lem:linking_pairing} we can pick a basis $(\beta_{i})$ for $H^{1}(\bm{\Sigma})\cong H_{1}(X_{\bm{\Sigma}})$ which is dual to $([b_{i}])$ with respect to the linking pairing i.e.:
	\begin{equation*}
	\linking(y,-)=\sum_{i} \linking(y,b_{i})\beta_{i}
	\end{equation*}
	for any $[y]\in H_{1}(X_{\bm{\Sigma}})$.
	We can now see that the maps $H_{1}(\bm{\Sigma}^{+}) \to H^{1}(\bm{\Sigma})$ and $H_{1}(\bm{\Sigma}^{-}) \to H^{1}(\bm{\Sigma})$ are given by
	\begin{align*}
		[b^{+}_{i}] &\longmapsto \linking(b^{+}_{i},-)=\sum_{j}\linking(b^{+}_{i},b_{j})\beta_{j}= \sum_{j}\linking(b_{j},b^{+}_{i})\beta_{j}=\sum_{j}v_{j,i}\beta_{j}\\
		\shortintertext{and}
		[b^{-}_{i}] &\longmapsto \linking(b^{-}_{i},-)=\sum_{j}\linking(b^{-}_{i},b_{j})\beta_{j}= \sum_{j}\linking(b_{i},b^{+}_{j})\beta_{j}=\sum_{j}v_{i,j}\beta_{j}
	\end{align*}
	respectively, where we have used \cref{lem:linking_changing_pushoffs}.
	We have now shown that the map
	\begin{equation*}
		H_{1}(\bm{\Sigma}^{+})\oplus H_{1}(\bm{\Sigma}^{-})\to H_{1}(\bm{\Sigma})\oplus H_{1}(X_{\bm{\Sigma}})\comma
	\end{equation*}
	with our choice of bases, is given by 
	\begin{equation*}
	\begin{bmatrix}
		I & I\\
		V^{\transpose} & V
	\end{bmatrix}\point
	\end{equation*}
	Using the previous argument, we can lift these bases to bases such that the map
	\begin{equation*}
		H_{1}(\bm{\Sigma}^{+})[t^{\pm1}]\oplus H_{1}(\bm{\Sigma}^{-})[t^{\pm1}]\to H_{1}(\bm{\Sigma})[t^{\pm1}]\oplus H_{1}(X_{\bm{\Sigma}})[t^{\pm1}]
	\end{equation*}
	from \eqref{eq:mayer-vietoris-top} is the map 
	\begin{equation*}
		\begin{bmatrix}
			I & I\\
			V^{\transpose} & tV
		\end{bmatrix}\comma
	\end{equation*}
	which presents $\A(K)$.
	We can now use the operations from \cref{thm:well_definedness_order_ideal} to get:
	\begin{equation*}
		\begin{bmatrix}
			I & I\\
			V^{\transpose} & tV
		\end{bmatrix}
		\stackrel{\ref{it:order_ideal_2}}{\leadsto}
		\begin{bmatrix}
			I & 0\\
			V^{\transpose} & tV -V^{\transpose}
		\end{bmatrix}
		\stackrel{\ref{it:order_ideal_4}}{\leadsto}
		\begin{bmatrix}
			I & 0\\
			V^{\transpose} & V^{\transpose}-tV
		\end{bmatrix}
		\stackrel{\ref{it:order_ideal_6}}{\leadsto}
		V^{\transpose}-tV\comma
	\end{equation*}
	so $V^{\transpose}-tV$ presents $\A(K)$ and we win!
\end{proof}

\begin{corollary}
	For any knot $K\colon S^{1} \mono S^{3}$, the order ideal $\order{\A(K)}$ of $\A(K)$ is principal.
\end{corollary}

\begin{proof}
	The matrix $V^{\transpose} -tV$ is square.
\end{proof}

\begin{remark*}
	Morally, this not a corollary, but a \emph{porism} (it follows from the \emph{proof}, not the \emph{statement}, of the theorem) -- we get a square presentation of $\A(K)$ directly from the Mayer--Vietoris sequence after we have showed, that $\tilde{\delta}$ in \eqref{eq:mayer-vietoris-top} is $0$.
\end{remark*}

Now, for the crowning jewel of this project:

\begin{definition}
	Any generator of $\order{\A(K)}$ is called the \emph{Alexander polynomial} of $K$ and is denoted by $\Delta_{K}(t)$.
\end{definition}
