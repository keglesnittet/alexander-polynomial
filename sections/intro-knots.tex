\section{Knots, first steps}
\label{sec:knots_first_steps}

Here we will take the first steps by defining knots and providing some of the essential tools for working with knots.

\subsection{Knots \& Links}
\label{ssec:knots_and_links}

The objects of study of this project are \emph{knots}.
In the literature, there are a plethora of (non-equivalent) definitions of ``knot''.
We will begin by giving a definition of a \emph{topological knot}: 

\begin{definition}
	A \emph{topological knot} is a topological embedding $K\colon S^{n} \mono X$ of a sphere $S^{n}$ into a topological space $X$.
	We say that two topological knots $K,K'\colon S^{n}\mono X$ are \emph{equivalent} if there exists a homeomorphism $h\colon X\to X$ such that $K'=h\circ K$.
	In this case, we call $h$ an equivalence between $K$ and $K'$.
\end{definition}

\begin{warning}
	This will \emph{not} be our working definition of a knot.
\end{warning}

The reason we will not focus on topological knots, is that they can be \emph{wild} (read: infinitely complicated, e.g.\ space-filling).
To avoid wild knots, one can put niceness restrictions on the knots e.g.\ requiring $X$ to be a topological (resp.\ smooth or piece-wise linear) manifold, the embedding be topologically locally flat (resp.\ smooth or piece-wise linear) and the equivalences be topologically locally flat (resp.\ smooth or piece-wise linear or ambient isotopies).
For added niceness one can also add orientations to $X$ and $S^{n}$ and require $h$ to respect them.
This leads us to our working definition of a knot:

\begin{definition}[Knot]\label{def:knot}
	Let $X$ be a smooth manifold.
	A (smooth) knot in $X$ is an oriented smooth embedding $K\colon S^{n} \mono X$.
	Two knots $K,K'\colon S^{n} \mono X$ are equivalent if there exists a diffeomorphism $h\colon X\to X$ such that $h$ respects the orientation and $K'=h\circ K$.
\end{definition}

Even though we will focus on the smooth case, a lot of our results work even in the topological case.
We \emph{highly} recommend the reader consider which cases the results presented in this project apply to.

We will mainly focus on the case $X=S^{n+2}$ and usually $n=1$.
The reason we direct our focus on codimension $2$ knots is that, in the piece-wise linear and topologically locally flat case, there is only one knot of codimension greater than $2$.
Furthermore, to quote Rolfsen \cite[8]{bog:Rolfsen}:

\begin{displayquote}
	$\ldots$ knot theory, as well as link theory, is also more-or-less trivial in codimension one.
\end{displayquote}

We would be amiss if we did not, in a project on knot theory, at least mention \emph{links}.
What could be better that one knot?
Multiple knots!
In \cref{def:knot}, replacing $S^{n}$ by $\coprod_{I} S^{k_{i}}$, giving $I$ an order and requiring $h$ respect that order gives the definition of a \emph{link} and equivalences thereof.

\subsection{First Steps}
\label{ssec:first_steps}

In this section we introduce some of the most basic tools of knot theory not all of which will be relevant for this project (but we feel an introduction to knot theory should at least mention these -- though, in our case, it messes with the structuring).

\subsubsection{Seifert surfaces}
\label{sssec:seifert_surfaces}

In this section we introduce \emph{Seifert surfaces}, which are an essential tool in the study of knots (and links).

\begin{definition}
	Let $Y$ be a smooth manifold and $X\subset Y$ a submanifold.
	A smooth embedding $b\colon X\times[-1,1] \to Y$ such that the restriction of $b$ to $X\times\{0\}$ is the inclusion $X\mono Y$ is called a \emph{bicollar} of $X$ (in $Y$).
	If such a bicollar exists, $X$ (and any embedding with image $X$) is said to be \emph{bicollared} (in $Y$).
\end{definition}

\begin{definition}
	Let $K\colon S^{n} \mono S^{n+2}$ be a knot.
	A connected, bicollared, compact, smooth manifold $M^{n+1}\subset S^{n+2}$ such that $\partial M = \im K$ (with the correct orientation) is called a \emph{Seifert surface} for $K$.
\end{definition}

\begin{remark*}
	Note that any Seifert surface is, in particular, orientable.
\end{remark*}

In \cref{ssec:existence_of_seifert_surfaces}, we will sketch a proof that there exists a Seifert surface for each (smooth) knot $S^{n} \mono S^{n+2}$.

\begin{definition}
	Let $M^{m}\subset N^{n}$ be smooth manifolds.
	A smooth embedding $t\colon M\times D^{n-m}\to N$ such that the restriction of $t$ to $M\times\{0\}$ is the inclusion $M\mono N$ is called a \emph{tubular neighbourhood} of $M$.
\end{definition}

\begin{remark*}
	Note that a bicollar is just a special case of a tubular neighbourhood.
\end{remark*}

We will use the following lemma without proof:

\begin{lemma}
	Let $K\colon S^{1}\mono S^{3}$ be a knot.
	There exists a tubular neighbourhood $\nu(K)\colon (\im K) \times D^{1}\to S^{3}$ of $K$.
\end{lemma}

Another obvious knot invariant is the following:

\begin{definition}
	The \emph{genus} of a knot $K$ is the minimal possible genus of a Seifert surface for $K$.
\end{definition}

\subsubsection{Knot invariants}
\label{sssec:knot_invariants}

Determining whether two knots are equivalent is non-trivial.
\emph{Knot invariants}, the hallmark of knot theory, help distinguish knots up to equivalence.
One can describe knot invariants as functions from (a subset of) the set of knots to a set $S$ sending equivalent knots to the same element of $S$.\footnote{or more often, to \emph{equivalent} elements of $S$ if one has a notion of equivalence in $S$.}
The following are examples of knot invariants:

\begin{definition}
	Let $K\colon S^{n} \mono S^{n+2}$ be a knot.
	\begin{liste}
		\item For $n=1$ the minimal number of self-intersections of a \emph{regular projection}\footnote{see \cref{def:regular_projection} -- though it is what you think it is.} of a knot equivalent to $K$, called the \emph{crossing number} of $K$.
		\item The group $\pi_{1}(S^{n+2}\uden\im K)$, called the \emph{knot group} of $K$.\qedhere
	\end{liste}
\end{definition}

These are only two invariants, but knot theory is \emph{full} of them.
The trade-off with knot invariants, are usually computability of the invariant vs.\ how sensitive it is, i.e.\ how ``many'' knots it can distinguish.
As mentioned, the main result of this project is the construction of the knot invariant called the \emph{Alexander polynomial}.
Probably the most important knot invariant is the \emph{knot complement}:

\begin{notation}
	Let $K\colon S^{n} \mono X$ be a knot.
	We will denote the complement $X \uden \im K$ by $K^{\complement}$.
\end{notation}

Since we focus on smooth knots, we will usually need to work with the following homotopic analogue of the knot complement:

\begin{notation}
	Let $K\colon S^{n} \mono X$ be a knot and $\nu(K)$ be a tubular neighbourhood of $K$.
	We will denote the complement $X \uden \interior{\nu}(K)$ by $X_{K}$ (suppressing $\nu$).
\end{notation}

\begin{remark*}
	Note, that $K^{\complement} \simeq X_{K}$.
\end{remark*}
