\section{Seifert Surfaces}
\label{sec:seifert_surfaces}

Having introduced Seifert surfaces in \cref{ssec:first_steps}, we will, in this section, sketch two proofs of their existence (in the smooth case) and use them to define the \emph{linking number} of disjoint knots.
We will then use these linking numbers to define \emph{Seifert matrices} for a knot.

\subsection{Existence of Seifert Surfaces}
\label{ssec:existence_of_seifert_surfaces}

In this section we will describe the Seifert algorithm, which produces a Seifert surface for a knot and then we will sketch a more formal proof of the existence of Seifert surfaces for knots.
This will also be one of the places in this project we restrict ourselves to the smooth category.

\subsubsection{Seifert algorithm}
\label{sssec:seifert_algorithm}

In this section we will relax our demand for formality and rely more on geometric intuition due to the pain and opacity of the alternative.

We will use the following definition (inspired by \cite[63]{bog:Rolfsen}) in the description of the algorithm:

\begin{definition}\label{def:regular_projection} 
	Let $K\colon S^{1} \mono \R^{3}$ be a knot and let $p\colon \R^{3} \to \R^{2}$ be the orthogonal projection onto a plane.
	If the composition $p\circ K\colon S^{1} \to \R^{2}$ never sends $3$ distinct points to the same point and only intersects itself transversely, we call $p$ a \emph{regular projection} of $K$.
\end{definition}

We will use, without proof, that for any knot $S^{1} \mono \R^{3}$ there exists such a regular projection of an equivalent knot.
The following algorithm is basically a reformulation of the proof in \cite[120]{bog:Rolfsen}.
We recommend the reader skim \cref{ex:seifert_alg_trefoil} before reading the Seifert algorithm.

\begin{algorithm}[Seifert]
	Let $K\colon S^{1} \mono \R^{3}$ be a (smooth) knot and fix a regular projection $p$ of (a knot equivalent to) $K$.
	Let $K'$ denote the image of $p$.
	\begin{algorithmsteps}
		\item Replace all crossings in $K'$ as depicted in \cref{fig:seifert_algorithm_cuts}.
	\end{algorithmsteps}
	\begin{figure}[h]\centering
		\vspace{-.7cm}
		\caption{Seifert algorithm step 1.}\label{fig:seifert_algorithm_cuts}
		\includegraphics[width=.6\textwidth,keepaspectratio]{Metapost/figures/seifert-alg-step1.mps}
	\end{figure}
	Now $K'$ is a collection of disjoint Jordan-curves in the plane.
	\begin{algorithmsteps}[resume]
		\item Orient the discs bounded by the Jordan-curves of $K'$, so that the boundary runs counterclockwise as seen from the ``$+$'' side.
		\item Offset each of the discs oriented in the previous step from the plane such that the innermost discs are on top.
		\item Connect the discs where the crossings were in the way depicted in \cref{fig:seifert_alg_step_end} producing a Seifert surface for $K$.
	\end{algorithmsteps}
	\begin{figure}[!h]\centering
		\vspace{-.7cm}
		\caption{Seifert algorithm step 4.}\label{fig:seifert_alg_step_end}
		\includegraphics[width=.6\textwidth,height=.6\textwidth,keepaspectratio]{Metapost/figures/seifert-alg-step-end.mps}
	\end{figure}
\end{algorithm}

\begin{example}[Seifert algorithm for the trefoil]\label{ex:seifert_alg_trefoil} 
	Taking $K\colon S^{1} \mono \R^{3}$ to be a trefoil, we illustrate the Seifert algorithm for $K$ in \cref{fig:trefoil}.
	\begin{figure}[!h]\centering
		\caption{Seifert algorithm on the trefoil}\label{fig:trefoil}
		\begin{subfigure}[h]{.4\textwidth}\centering
			\caption{Regular projection of a trefoil.}
			\includegraphics[keepaspectratio]{Metapost/figures/trefoil-step1.mps}
		\end{subfigure}\hspace{1cm}
		\begin{subfigure}[h]{.4\textwidth}\centering
			\caption{Step 1 and 2.}\label{fig:seifert_algorithm_step_1}
			\includegraphics[keepaspectratio]{Metapost/figures/trefoil-step2.mps}
		\end{subfigure}
		\begin{subfigure}[h]{.4\textwidth}\centering
			\caption{Step 3. The grey area is supposed to depict a \emph{shadow}.}\label{fig:seifert_algorithm_step_3}
			\includegraphics[keepaspectratio]{Metapost/figures/trefoil-step3.mps}
		\end{subfigure}\hspace{1cm}
		\begin{subfigure}[h]{.4\textwidth}\centering
			\caption{Step 4. Here, the color is to differentiate between the ``$+$'' and ``$-$'' side.}\label{fig:seifert_algorithm_step_4}
			\includegraphics[keepaspectratio]{Metapost/figures/trefoil-step4.mps}
		\end{subfigure}
	\end{figure}
\end{example}

\subsubsection{Alexander duality}
\label{sssec:alexander--poincaré_duality}

We will now sketch a proof of the existence of Seifert surfaces using Alexander duality.
In the proof, we rely on multiple results we will not prove, including Alexander duality.

The proof uses a method for abstractly constructing a Seifert surface for any knot $S^{n} \mono S^{n+2}$.
Also, the method used to construct the Seifert surface yields \emph{all} Seifert surfaces while the Seifert algorithm does not.

We will use the following formulation of Alexander duality from \cite[Corollary~3.45]{bog:Hatcher}, which this project, unfortunately, does not contain a proof of:

\begin{theorem}[Alexander duality]\label{thm:alexander_duality}
	If $K$ is a	locally contractible, compact, non-empty, proper subspace of $S^{n}$ then 
	\begin{equation*}
		\tilde{H}_{i}(K;\Z) \cong \tilde{H}^{n-i-1}(S^{n}\uden K;\Z)
	\end{equation*}
	for all $i\in\Z$.
\end{theorem}

\begin{proof}
	Omitted.
\end{proof}

\begin{theorem}
	If $K\colon S^{n} \mono S^{n+2}$ is a (smooth) knot, then there exists a Seifert surface for $K$.
\end{theorem}

\begin{proof}[Sketch of proof]
Let $K\colon S^{n} \mono S^{n+2}$ be a knot.
Pick a tubular neighbourhood $\nu(K)$ of $K$.
Pick a non-zero element 
\begin{equation*}
	[\gamma]\in H_{1}(\nu(K);\Z)\cong\Z,\quad [\gamma]\neq0\point
\end{equation*}
Note that $\nu(K)$ is a locally contractible, compact, nonempty, proper subspace of $S^{n+2}$ so Alexander duality gives us, in particular, that
\begin{equation*}
	\tilde{H}_{1} (\nu(K);\Z) \cong \tilde{H}^{1}(X_{K};\Z) = H^{1}(X_{K};\Z) \cong [X_{K},K(\Z,1)] \cong [X_{K},S^{1}]\comma
\end{equation*}
where we have used that singular cohomology is represented by Eilenberg--MacLane spectra,%
\footnote{in the sense of \cite[theorem 3.4.2]{bog:CatTop} stating that $H^{n}(X;G)\cong [X,K(G,n)]$ for all $n\geq0$.}
and that $S^{1}$ is a $K(\Z,1)$.
Sending $[\gamma]$ through the composition of these isomorphisms we thereby get a homotopy-class of maps $[f]\in[S^{n+2}\uden \interior{\nu}(K),S^{1}]$.
Using smooth approximation, we may and do assume $f$ is smooth.
We may and do also assume $\im f$ has non-zero Lebesgue measure.
Now, using Sards theorem \cite[10]{bog:Milnor} we can pick a regular point of $f$ in $\im f$ and assume WLOG that it is the point $1\in S^{1}$.
Using transversality we can assume $f$ is transversal to $1$.
This now gives us, that $\Sigma\coloneqq f^{-1}(1)$ is a codimension $1$ smooth manifold of $S^{n+2}$ (by e.g.\ \cite[Theorem~15.3]{bog:Bredon1993topology}).

To make sure $f$ behaves properly at $\partial(S^{n+2}\uden\interior{\nu}(K))$ we note that all the tools we have used have relative counterparts, so we could have done this entire construction relatively with $(X_{K},\partial X_{K})$.
From doing this, we can get a codimension $1$ smooth submanifold $\Sigma$ of $S^{n+2}$ with boundary $K$ (orientation also follows from the construction).
The smooth manifold $\Sigma$ may not be path-connected, but we can throw away all path-components of $\Sigma$ not containing $K$ and get a Seifert surface for $K$.
\end{proof}

\subsection{Linking Number}
\label{ssec:linking_number}

In this section we will define a link-invariant called the \emph{linking number} (even for topological links).

\begin{definition}
	Let $J,K\colon S^{1} \mono S^{3}$ be knots.
	We say that $J$ and $K$ are \emph{disjoint} if $\im J$ and $\im K$ are disjoint.
\end{definition}

We now want to define the \emph{linking number} of two disjoint knots.
Since there are many equivalent definitions of the linking number we will give two different constructions, show that they give the same number (with a sign convention) and then define this to be the linking number.
We only give two different constructions, but we highly recommend the reader take a look at \cite[132-133]{bog:Rolfsen}, where Rolfsen gives \emph{eight} equivalent definitions of the linking number -- number seven of which being particularly entertaining and discovered by non other than Gauß.

\begin{notation}
	We let $\hurewicz$ denote the Hurewicz homomorphism.
\end{notation}

\begin{construction}\label{con:linking_number}
	Let $J,K\colon S^{1} \mono S^{3}$ be disjoint topological knots.
	\begin{enumerate}[label=$\linking_{\arabic*}\colon$]
		\item Since $J$ and $K$ are disjoint, we have $\im J\subset K^{\complement}$ and so (since $H_{1}(K^{\complement};\Z)\cong\Z$ from \cref{thm:reduced_hom_compl_spheres}) we  can pick a generator $g$ of $H_{1}(K^{\complement};\Z)$ and define $\link[1]{J}{K}$ to be the integer $n$ such that $[J]=ng$, where $[J]$ denotes the homology class of $J$ in $K^{\complement}$.
		\item Fix an isomorphism $\phi\colon H_{1}(K^{\complement};\Z)\cong \Z$.
		Since $J$ and $K$ are disjoint, we have $\im J\subset K^{\complement}$ pick a point $x\in\im J$ we can define $\link[2]{J}{K} \coloneqq \phi(\hurewicz([J]))$ where $\hurewicz\colon \pi_{1}(K^{\complement},x) \to H_{1}(K^{\complement};\Z)$ is the Hurewicz homomorphism.
	\end{enumerate}
\end{construction}

\begin{lemma}\label{lem:linking_well-defined}
	The maps $\linking_{1}$ and $\linking_{2}$ in \cref{con:linking_number} are well-defined and agree up to sign, i.e.\ $\link[1]{J}{K}=\pm\link[2]{J}{K}$ for all disjoint knots $J,K\colon S^{1} \mono S^{3}$.
\end{lemma}

\begin{proof}
	Well-definedness of $\linking_{2}$ is trivial.
	That $\linking_{1}=\pm\linking_{2}$ is basically by definition; $\hurewicz$ is just the abelianization.
	We pick a sign-convention such that $\linking_{1}=\linking_{2}$.
	Now well-definedness of $\linking_{1}$ follows from $\linking_{1}=\linking_{2}$.
\end{proof}

\begin{definition}
	Let $J,K\colon S^{1}\mono S^{3}$ be disjoint knots.
	We define the \emph{linking number} of $J$ and $K$ to be $\link[i]{J}{K}$ for $i=1,2$ and denote it $\link{J}{K}$.
	We immediately extend $\linking$ bilinearly to disjoint unions of simple curves, and get the \emph{linking pairing} by noting that $\linking(J,K)=\linking(K,J)$ (from geometric intuition) and using $\linking_{1}$ (which is already linear in one variable).
\end{definition}

\subsection{Seifert Matrices}
\label{ssec:seifert_matrices}

In this section we will introduce Seifert matrices following \cite[201]{bog:Rolfsen}.
A bicollar allows us to do the following:

\begin{notation}
	Let $M\subset N$ be manifolds, $b$ be a bicollar of $M$ in $N$ and let $x\colon X\to M$ be a map.
	We denote by $x^{+}$ (suppressing $b$) the map $b\circ(x,1)\colon X \to N$ and likewise by $x^{-}$ the map $b\circ(x,-1)\colon X \to N$.
	We call $x^{+}$ the \emph{pushoff} of $x$.
\end{notation}

\begin{definition}
	Let $K\colon S^{1} \mono S^{3}$ be a knot, $\Sigma_{g}$ a Seifert surface for $K$ and $b$ a bicollar of $\Sigma_{g}$.
	We call a map $H_{1}(\interior{\Sigma}_{g};\Z) \times H_{1}(\interior{\Sigma}_{g};\Z) \to \Z$ defined on $1$-cycles $[x],[y]$ by
	\begin{equation*}
		([x],[y]) \mapsto \link{x}{y^{+}}
	\end{equation*}
	a \emph{Seifert form} of $K$ (this is well-defined, because we can fix one entry at a time).
\end{definition}

We will need the following lemma for our presentation of the Alexander module.

\begin{lemma}\label{lem:linking_changing_pushoffs}
	Let $K\colon S^{1} \mono S^{3}$ be a knot and $\Sigma_{g}$ a Seifert surface for $K$.
	For all $[x],[y]\in H_{1}(\interior{\Sigma}_{g})$ we have: 
	\begin{equation*}
		\link{x^{-}}{y} = \link{x^{-}}{y^{+}} = \link{x}{y^{+}}\point
	\end{equation*}
\end{lemma}

\begin{proof}
	This follows easily, by using $\linking_{1}$ and $b$ to form a $1$-boundary making $[y]=[y^{+}]$ in $H_{1}((x^{-})^{\complement})$ and $[x]=[x^{-}]$ in $H_{1}((y^{+})^{\complement})$.
\end{proof}

\begin{definition}
	Let $f\colon H_{1}(\interior{\Sigma}_{g};\Z) \times H_{1}(\interior{\Sigma}_{g};\Z) \to \Z$ be a Seifert form of $K$.
	Choosing a basis $([e_{1}],[e_{2}],\ldots [e_{2g}])$ for $H_{1}(\interior{\Sigma}_{g};\Z)\cong \Z^{2g}$ we associate to $f$ the matrix $V=(v_{i,j})\in\Mat_{\Z}(2g,2g)$ defined by $v_{i,j}=\link{e_{i}}{e_{j}^{+}}$.
	We call such a matrix $V$ a \emph{Seifert matrix} (for $K$).
\end{definition}

\begin{remark*}
	A Seifert matrix is not a \emph{knot} invariant.
	It depends on the choice of Seifert surface and bicollar.
\end{remark*}

\begin{warning}
	For a fixed Seifert form $f$, Seifert matrices are only preserved up to congruence:
	Let $V$ be a Seifert matrix for $f$.
	A matrix $W$ over $\Z$ is a Seifert matrix for $f$ if and only if $W=PVP^{\transpose}$ for some invertible matrix $P$ over $\Z$.
\end{warning}

\begin{lemma}\label{lem:linking_pairing}
	Let $K\colon S^{1} \mono S^{3}$ be a knot, and $\Sigma_{g}$ a Seifert surface for $K$.
	The isomorphism $H_{1}(S^{3}\uden \Sigma_{g};\Z) \to H^{1}(\Sigma_{g};\Z)=\Hom(H_{1}(\Sigma_{g};\Z),\Z)$ produced by Alexander duality is given by the linking pairing, i.e.: 
	\begin{equation*}
		\linking(-,-) \colon H_{1}(S^{3}\uden \Sigma_{g};\Z) \times H_{1}(\Sigma_{g}) \to \Z\point
	\end{equation*}
\end{lemma}

\begin{proof}
	Omitted.
	The proof amounts to unravelling the definition of the isomorphism provided by Alexander duality.
\end{proof}
